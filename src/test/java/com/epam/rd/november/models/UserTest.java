package com.epam.rd.november.models;

import com.epam.rd.november.models.builders.UserBuilder;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

	@Test
	void getFirstName() {
		User user = new UserBuilder()
				.firstName("Ivan")
				.lastName("Petrov")
				.email("ivan@gmail.com")
				.password("0000")
				.createUser();
		assertEquals("Ivan", user.getFirstName());
	}
}