package com.epam.rd.november.controller;

import com.epam.rd.november.resource.ConfigManager;
import org.junit.Test;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class ManagerServletTest {

    private HttpServletRequest req = mock(HttpServletRequest.class);
    private HttpServletResponse resp = mock(HttpServletResponse.class);
    private RequestDispatcher dispatcher = mock(RequestDispatcher.class);
    private String context = "/rental-car";
    private ServletContext mockServletContext = mock(ServletContext.class);


    @Test
    public void emptyURL() throws ServletException, IOException {
        ManagerServlet servlet = new ManagerServlet();
        ManagerServlet spy = spy(servlet);
        when(req.getRequestURI()).thenReturn(context + "");
        when(req.getMethod()).thenReturn("get");
        doReturn(mockServletContext).when(spy).getServletContext();
        doReturn(dispatcher).when(mockServletContext).getRequestDispatcher(ConfigManager.getProperty("path.page.index"));
        spy.doGet(req, resp);
        verify(dispatcher, times(1)).forward(req, resp);
    }

    @Test
    public void wrongCommandURL() throws ServletException, IOException {
        ManagerServlet servlet = new ManagerServlet();
        ManagerServlet spy = spy(servlet);
        when(req.getRequestURI()).thenReturn(context + "/loginnn");
        when(req.getMethod()).thenReturn("get");
        doReturn(mockServletContext).when(spy).getServletContext();
        doReturn(dispatcher).when(mockServletContext).getRequestDispatcher(ConfigManager.getProperty("path.page.index"));
        spy.doGet(req, resp);
        verify(dispatcher, times(1)).forward(req, resp);
    }

    @Test
    public void validCommandURL() throws ServletException, IOException {
        ManagerServlet servlet = new ManagerServlet();
        ManagerServlet spy = spy(servlet);
        when(req.getRequestURI()).thenReturn(context + "/login");
        when(req.getMethod()).thenReturn("get");
        doReturn(mockServletContext).when(spy).getServletContext();
        doReturn(dispatcher).when(mockServletContext).getRequestDispatcher(ConfigManager.getProperty("path.page.login"));
        spy.doGet(req, resp);
        verify(dispatcher, times(1)).forward(req, resp);
    }
}