package com.epam.rd.november.comands.orders;

import com.epam.rd.november.resource.ConfigManager;
import com.epam.rd.november.resource.MessageManager;
import org.junit.jupiter.api.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MakeOrderCommandTest {
    private HttpServletRequest req = mock(HttpServletRequest.class);
    private HttpSession session = mock(HttpSession.class);
    private MakeOrderCommand command = new MakeOrderCommand();

    @Test
    public void emptyFormParams() {
        when(req.getSession()).thenReturn(session);
        when(session.getAttribute("userId")).thenReturn(1);
        when(req.getParameter("city")).thenReturn("");
        when(req.getParameter("address")).thenReturn("");
        when(req.getParameter("document")).thenReturn("");
        String page = command.execute(req);
        assertEquals(ConfigManager.getProperty("path.page.order"), page);
    }

    @Test
    public void invalidCity() {
        when(req.getSession()).thenReturn(session);
        when(session.getAttribute("userId")).thenReturn(2);
        when(req.getParameter("city")).thenReturn("DS");
        when(req.getParameter("address")).thenReturn("Lenina 34/2");
        when(req.getParameter("document")).thenReturn("AN 434343");
        String page = command.execute(req);
        verify(req).setAttribute("errorMessage", MessageManager.getProperty("message.invalid.city"));
        assertEquals(ConfigManager.getProperty("path.page.order"), page);
    }

    @Test
    public void invalidAddress() {
        when(req.getSession()).thenReturn(session);
        when(session.getAttribute("userId")).thenReturn(3);
        when(req.getParameter("city")).thenReturn("Dnipro");
        when(req.getParameter("address")).thenReturn("Lenina-Lenina");
        when(req.getParameter("document")).thenReturn("AN 434343");
        String page = command.execute(req);
        verify(req).setAttribute("errorMessage", MessageManager.getProperty("message.invalid.address"));
        assertEquals(ConfigManager.getProperty("path.page.order"), page);
    }

    @Test
    public void invalidDocument() {
        when(req.getSession()).thenReturn(session);
        when(session.getAttribute("userId")).thenReturn(4);
        when(req.getParameter("city")).thenReturn("Dnipro");
        when(req.getParameter("address")).thenReturn("Lenina 34/2");
        when(req.getParameter("document")).thenReturn("AN 43");
        String page = command.execute(req);
        verify(req).setAttribute("errorMessage", MessageManager.getProperty("message.invalid.document"));
        assertEquals(ConfigManager.getProperty("path.page.order"), page);
    }
}