package com.epam.rd.november.comands.user;

import com.epam.rd.november.comands.user.RegisterCommand;
import com.epam.rd.november.resource.ConfigManager;
import com.epam.rd.november.resource.MessageManager;
import org.junit.jupiter.api.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class RegisterCommandTest {
	private HttpServletRequest req = mock(HttpServletRequest.class);
	private HttpSession session = mock(HttpSession.class);
	private RegisterCommand command = new RegisterCommand();

	@Test
	public void emptyFormParams() {
		when(req.getParameter("login")).thenReturn("");
		when(req.getParameter("email")).thenReturn("");
		when(req.getParameter("password")).thenReturn("");
		when(req.getParameter("firstname")).thenReturn("");
		when(req.getParameter("lastname")).thenReturn("");
		String page = command.execute(req);
		assertEquals(ConfigManager.getProperty("path.page.registration"), page);
	}

	@Test
	public void invalidPassword() {
		when(req.getParameter("login")).thenReturn("alex");
		when(req.getParameter("email")).thenReturn("alex@gmail.com");
		when(req.getParameter("password")).thenReturn("fgfgfgfgf");
		when(req.getParameter("firstname")).thenReturn("Alex");
		when(req.getParameter("lastname")).thenReturn("Tyler");
		String page = command.execute(req);
		verify(req).setAttribute("errorMessage", MessageManager.getProperty("message.invalid.password"));
		assertEquals(ConfigManager.getProperty("path.page.registration"), page);
	}

	@Test
	public void invalidEmail() {
		when(req.getParameter("login")).thenReturn("alex");
		when(req.getParameter("email")).thenReturn("alexgmail.com");
		when(req.getParameter("password")).thenReturn("Alex545");
		when(req.getParameter("firstname")).thenReturn("Alex");
		when(req.getParameter("lastname")).thenReturn("Tyler");
		String page = command.execute(req);
		verify(req).setAttribute("errorMessage", MessageManager.getProperty("message.invalid.email"));
		assertEquals(ConfigManager.getProperty("path.page.registration"), page);
	}


}