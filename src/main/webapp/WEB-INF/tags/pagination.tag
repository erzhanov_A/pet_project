<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="paginator">
    <table>
        <tr>
            <c:forEach begin="1" end="${numberOfPages}" var="i">
                <c:choose>
                    <c:when test="${currentPage eq i}">
                        <td>${i}</td>
                    </c:when>
                    <c:otherwise>
                        <td><a href="${pageContext.request.contextPath}/cars?page=${i}">${i}</a></td>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </tr>

        <tr>
            <c:if test="${currentPage != 1}">
                <td><a href="${pageContext.request.contextPath}/cars?page=${currentPage - 1}"><fmt:message key="previous" bundle="${msg}" /></a></td>
            </c:if>
            <c:if test="${currentPage lt numberOfPages}">
                <td><a href="${pageContext.request.contextPath}/cars?page=${currentPage + 1}"><fmt:message key="next" bundle="${msg}" /></a></td>
            </c:if>
        </tr>
    </table>
</div>