<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <fmt:setLocale value="${sessionScope['locale']}"/>
    <fmt:setBundle basename="${sessionScope['bundleFile']}" var="msg"/>
    <title>Block/Unblock</title>
</head>
<body>
<jsp:include page="/jsp/parts/header.jsp"/>

<section>
    <h1><fmt:message key="update.user.status" bundle="${msg}" /></h1>
    <p><fmt:message key="update.user.status.message" bundle="${msg}" /></p>

    <form method="post" class="updatestatus" action="${pageContext.request.contextPath}/block" style="margin: 10px">
        <input type="hidden" value="${blockUser}" name="blockUser">
        <div>
            <select name="userStatus">
                <option value=" "><fmt:message key="user.status" bundle="${msg}" /></option>
                <option value="Active"><fmt:message key="user.active" bundle="${msg}" /></option>
                <option value="Blocked"><fmt:message key="user.blocked" bundle="${msg}" /></option>
            </select>
        </div>
        <div>
            <fmt:message key="submit" bundle="${msg}" var="submit"/>
            <input type="submit" value="${submit}">
        </div>
    </form>
</section>

<jsp:include page="/jsp/parts/footer.jsp"/>
</body>
</html>
