<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <fmt:setLocale value="${sessionScope['locale']}"/>
    <fmt:setBundle basename="${sessionScope['bundleFile']}" var="msg"/>
    <title>Create Car</title>
</head>
<body>
<jsp:include page="/jsp/parts/header.jsp"/>

<section>
    <h1><fmt:message key="create.car" bundle="${msg}" /></h1>
    <p><fmt:message key="create.car.message" bundle="${msg}" /></p>

    <form method="post" class="createcar" action="${pageContext.request.contextPath}/newcar" style="margin: 10px">
        <div>
            <label for="carmake"><fmt:message key="make.msg" bundle="${msg}" /></label>
            <input id="carmake" type="text" name="carmake" minlength="4" required> <br/>
        </div>
        <div>
            <label for="carmodel"><fmt:message key="model.msg" bundle="${msg}" /></label>
            <input id="carmodel" type="text" name="carmodel" minlength="1" required><br/>
        </div>
        <div>
            <label for="price"><fmt:message key="price.per.day"  bundle="${msg}" /></label>
            <input id="price" type="text" name="price" minlength="1" required><br/>
        </div>
        <div>
            <select name="cartype">
                <option value=" "><fmt:message key="type.msg" bundle="${msg}" /></option>
                <option value="Sedan"><fmt:message key="car.sedan" bundle="${msg}" /></option>
                <option value="Wagon"><fmt:message key="car.wagon" bundle="${msg}" /></option>
                <option value="Coupe"><fmt:message key="car.coupe" bundle="${msg}" /></option>
                <option value="SUV"><fmt:message key="car.suv" bundle="${msg}" /></option>
                <option value="Luxury"><fmt:message key="car.luxury" bundle="${msg}" /></option>
                <option value="Sportcar"><fmt:message key="sport.car" bundle="${msg}" /></option>
                <option value="Electric"><fmt:message key="car.electric" bundle="${msg}" /></option>
            </select>
        </div>

        <div>
            <fmt:message key="submit" bundle="${msg}" var="submit"/>
            <input type="submit" value="${submit}">
        </div>
    </form>
</section>

<jsp:include page="/jsp/parts/footer.jsp"/>
</body>
</html>
