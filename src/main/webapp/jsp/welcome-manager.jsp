<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <fmt:setLocale value="${sessionScope['locale']}"/>
    <fmt:setBundle basename="${sessionScope['bundleFile']}" var="msg"/>
    <title>Manager</title>
</head>
<body>
<jsp:include page="/jsp/parts/header.jsp"/>

    <h1><fmt:message key="hello.manager" bundle="${msg}"/></h1> <inf:UserInfo />
    <p><a href="${pageContext.request.contextPath}/orders"><fmt:message key="get.orders" bundle="${msg}"/></a></p>


<jsp:include page="/jsp/parts/footer.jsp"/>
</body>
</html>
