<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tagFile" tagdir="/WEB-INF/tags" %>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <fmt:setLocale value="${sessionScope['locale']}"/>
    <fmt:setBundle basename="${sessionScope['bundleFile']}" var="msg"/>
    <title>Orders</title>
</head>
<body>
<jsp:include page="/jsp/parts/header.jsp"/>

<section>
    <h1><fmt:message key="orders" bundle="${msg}" /></h1>

    <div style="overflow-x:auto;">
        <table  id="orders">
            <tr>
                <th><fmt:message key="orders.id" bundle="${msg}" /></th>
                <th><fmt:message key="orders.user" bundle="${msg}" /></th>
                <th><fmt:message key="document" bundle="${msg}" /></th>
                <th><fmt:message key="orders.car" bundle="${msg}" /></th>
                <th><fmt:message key="orders.delivery" bundle="${msg}" /></th>
                <th><fmt:message key="damageSumm" bundle="${msg}" /></th>
                <th><fmt:message key="isPaid" bundle="${msg}" /></th>
                <th><fmt:message key="orders.status" bundle="${msg}" /></th>
                <th><fmt:message key="orders.created" bundle="${msg}" /></th>
                <th><fmt:message key="orders.firstDay" bundle="${msg}" /></th>
                <th><fmt:message key="orders.lastDay" bundle="${msg}" /></th>
                <th><fmt:message key="with.driver" bundle="${msg}" /></th>
                <th><fmt:message key="orders.description" bundle="${msg}" /></th>
                <th><fmt:message key="orders.bill" bundle="${msg}" /></th>
                <th> </th>
                <th> </th>
            </tr>
            <c:forEach items="${orders}" var="user">
                <tr>
                <td>${user.id}</td>
                <td>${(user.user.firstName)} ${(user.user.lastName)}</td>
                <td>${(user.user.document)}</td>
                <td>${(user.car.make)} ${(user.car.model)}</td>
                <td>${(user.address.city)} ${(user.address.address)}</td>
                <td>${(user.crash.damageSum)}</td>
                <td>${(user.crash.paid)}</td>
                <td>${user.status}</td>
                <td>${user.created}</td>
                <td>${user.firstDay}</td>
                <td>${user.lastDay}</td>
                <td>${user.withDriver}</td>
                <td>${user.description}</td>
                <td>${user.bill}</td>
                <td>
                    <a href="${pageContext.request.contextPath}/addcrash?orderId=${(user.id)}"><fmt:message key="addcrash" bundle="${msg}" /></a>
                </td>
                <td>
                    <a href="${pageContext.request.contextPath}/updatestatus?updateStatusId=${(user.id)}"><fmt:message key="update.status" bundle="${msg}" /></a>
                </td>
                </tr>
            </c:forEach>
        </table>
    </div>
</section>

<tagFile:pagination/>

<jsp:include page="/jsp/parts/footer.jsp"/>
</body>
</html>