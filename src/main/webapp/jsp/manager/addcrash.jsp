<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <fmt:setLocale value="${sessionScope['locale']}"/>
    <fmt:setBundle basename="${sessionScope['bundleFile']}" var="msg"/>
    <title>Add Crash</title>

    <script type="text/javascript">
        function refreshCheckboxValue(checkbox){
            checkbox.value = checkbox.checked;
        }
    </script>
</head>
<body>
<jsp:include page="/jsp/parts/header.jsp"/>

<section>
    <h1><fmt:message key="addcrash" bundle="${msg}" /></h1>
    <p><fmt:message key="addcrash.message" bundle="${msg}" /></p>

    <form method="post" class="addcrash" action="${pageContext.request.contextPath}/addcrash" style="margin: 10px">
        <input type="hidden" value="${orderId}" name="orderId">
        <div>
            <label for="damageSum"><fmt:message key="damageSumm" bundle="${msg}" /></label>
            <input id="damageSum" type="text" name="damageSum" minlength="1" required> <br/>
        </div>
        <div>
            <label for="idPaid"><fmt:message key="isPaid" bundle="${msg}" /></label>
            <input id="idPaid" type="checkbox" name="idPaid" onclick="refreshCheckboxValue(this)" value="false"/>
        </div>
        <div>
            <textarea name="crashDescription" id='crashDescription'><c:out value="${desk}" /></textarea>
        </div>

        <div>
            <fmt:message key="submit" bundle="${msg}" var="submit"/>
            <input type="submit" value="${submit}">
        </div>
    </form>
</section>

<jsp:include page="/jsp/parts/footer.jsp"/>
</body>
</html>
