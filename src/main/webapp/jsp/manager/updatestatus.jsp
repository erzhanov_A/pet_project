<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <fmt:setLocale value="${sessionScope['locale']}"/>
    <fmt:setBundle basename="${sessionScope['bundleFile']}" var="msg"/>
    <title>Update status</title>
</head>
<body>
<jsp:include page="/jsp/parts/header.jsp"/>

<section>
    <h1><fmt:message key="update.status" bundle="${msg}" /></h1>
    <p><fmt:message key="update.status.message" bundle="${msg}" /></p>

    <form method="post" class="updatestatus" action="${pageContext.request.contextPath}/updatestatus" style="margin: 10px">
        <input type="hidden" value="${updateStatusId}" name="updateStatusId">

        <div>
            <select name="status">
                <option value=" "><fmt:message key="orders.status" bundle="${msg}" /></option>
                <option value="CREATED"><fmt:message key="order.created" bundle="${msg}" /></option>
                <option value="ACCEPTED"><fmt:message key="order.accepted" bundle="${msg}" /></option>
                <option value="REJECTED"><fmt:message key="order.rejected" bundle="${msg}" /></option>
                <option value="PAID"><fmt:message key="order.paid" bundle="${msg}" /></option>
                <option value="INPROGRESS"><fmt:message key="order.inprogress" bundle="${msg}" /></option>
                <option value="CLOSED"><fmt:message key="order.closed" bundle="${msg}" /></option>
            </select>
        </div>
        <div>
            <textarea name="description" id='description'><c:out value="${desk}" /></textarea>
        </div>
        <div>
            <fmt:message key="submit" bundle="${msg}" var="submit"/>
            <input type="submit" value="${submit}">
        </div>
    </form>
</section>

<jsp:include page="/jsp/parts/footer.jsp"/>
</body>
</html>
