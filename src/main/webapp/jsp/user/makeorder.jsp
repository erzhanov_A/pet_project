<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <fmt:setLocale value="${sessionScope['locale']}"/>
    <fmt:setBundle basename="${sessionScope['bundleFile']}" var="msg"/>
    <title>Order</title>

    <script type="text/javascript">
        function refreshCheckboxValue(checkbox){
            checkbox.value = checkbox.checked;
        }
    </script>
</head>
<body>
<jsp:include page="/jsp/parts/header.jsp"/>


<section>
    <h3><fmt:message key="order.title" bundle="${msg}"/></h3>
    <p><fmt:message key="order.message" bundle="${msg}" /></p>
    <form method="post" action="${pageContext.request.contextPath}/neworder" >
        <input type="hidden" value="${carId}" name="carId">
        <div>
            <label for="document"><fmt:message key="document" bundle="${msg}" /></label>
            <input id="document" type="text" name="document" minlength="6" required> <br/>
        </div>
        <div>
            <label for="firstdate"><fmt:message key="firstdate" bundle="${msg}" /></label>
            <input id="firstdate" type="date" min="2018-01-01" name="firstdate" required><br/>
        </div>
        <div>
            <label for="lastdate"><fmt:message key="lastdate" bundle="${msg}" /></label>
            <input id="lastdate" type="date" name="lastdate" min="2018-01-01" max="2020-01-01" required><br/>
        </div>
        <div>
            <label for="city"><fmt:message key="city" bundle="${msg}" /></label>
            <input id="city" type="text" name="city" minlength="3"> <br/>
        </div>
        <div>
            <label for="address"><fmt:message key="address" bundle="${msg}" /></label>
            <input id="address" type="text" name="address" minlength="3"> <br/>
        </div>
        <div>
            <label for="isWithDriver"><fmt:message key="with.driver" bundle="${msg}" /></label>
            <input id="isWithDriver" type="checkbox" name="isWithDriver" onclick="refreshCheckboxValue(this)" value="false"/>
        </div>
            <div>
                <textarea name="description" id='description'><c:out value="${desk}" /></textarea>
            </div>
        <div>
            <fmt:message key="submit" bundle="${msg}" var="submit"/>
            <input type="submit" value="${submit}">
        </div>

        <div>
            <p style="color: #c9590c"><fmt:message key="${errorMessage}" bundle="${msg}" /></p>
        </div>
    </form>
</section>

<jsp:include page="/jsp/parts/footer.jsp"/>
</body>
</html>
