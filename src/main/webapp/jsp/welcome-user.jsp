<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="inf" uri="taglib" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <fmt:setLocale value="${sessionScope['locale']}"/>
    <fmt:setBundle basename="${sessionScope['bundleFile']}" var="msg"/>
    <title>User</title>
</head>
<body>
<jsp:include page="/jsp/parts/header.jsp"/>

    <h1><fmt:message key="hello.user" bundle="${msg}"/></h1> <inf:UserInfo />

    <p><a href="${pageContext.request.contextPath}/cars"><fmt:message key="get.cars" bundle="${msg}"/></a></p>
    <p><a href="${pageContext.request.contextPath}/userorders"><fmt:message key="orders" bundle="${msg}"/></a></p>
    <p><a href="${pageContext.request.contextPath}/updateuser"><fmt:message key="update.user" bundle="${msg}"/></a></p>
    <p><a href="${pageContext.request.contextPath}/deleteuser"><fmt:message key="delete.user" bundle="${msg}"/></a></p>

<jsp:include page="/jsp/parts/footer.jsp"/>
</body>
</html>
