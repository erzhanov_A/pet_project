package com.epam.rd.november.dao.impl;

import com.epam.rd.november.dao.Dao;
import com.epam.rd.november.models.Crash;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Dao class for Crash entity.
 */
public class CrashDao extends Dao<Crash> {
    @Override
    public void setParams(Crash entity, PreparedStatement prst, boolean isUpdate) throws SQLException {
        prst.setInt(1, entity.getDamageSum());
        prst.setString(2, entity.getCrashDescription());
        prst.setBoolean(3, entity.isPaid());
        if (isUpdate) {
            prst.setInt(4, entity.getId());
        }

    }

    @Override
    public Crash getResult(ResultSet resultSet) throws SQLException {
        Crash crash = new Crash();
        crash.setId(resultSet.getInt("rId"));
        crash.setDamageSum(resultSet.getInt("damageSum"));
        crash.setCrashDescription(resultSet.getString("crashDescription"));
        crash.setIsPaid(resultSet.getBoolean("isPaid"));
        return crash;
    }

    public boolean create(Crash entity) {
        String createQuery = "INSERT INTO crash (damageSum, crashDescription, isPaid) VALUES (?, ?, ?)";
        return super.create(entity, createQuery);
    }

    public Crash read(int id) {
        String readQuery = "SELECT * FROM crash WHERE rId = ?";
        return super.read(id, readQuery);
    }

    public boolean update(Crash entity) {
        String updateQuery = "UPDATE crash SET damageSum = ?, crashDescription = ?, isPaid = ? WHERE rId = ?";
        return super.update(entity, updateQuery);
    }

    public boolean delete(int id) {
        String deleteQuery = "DELETE FROM crash WHERE rId=?";
        return super.delete(id, deleteQuery);
    }

    public List<Crash> getAll() {
        String getAllQuery = "SELECT * FROM crash";
        return super.getAll(getAllQuery);
    }

    public int getCount() {
        String countQuery = "SELECT COUNT(*) FROM crash";
        return super.getCount(countQuery);
    }
}
