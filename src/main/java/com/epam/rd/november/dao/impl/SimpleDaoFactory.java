package com.epam.rd.november.dao.impl;

import com.epam.rd.november.dao.DaoFactory;

public class SimpleDaoFactory implements DaoFactory {
    private static final SimpleDaoFactory factory = new SimpleDaoFactory();
    private final UserDao userdao = new UserDao();
    private final CarDao cardao = new CarDao();
    private final CrashDao crashDao = new CrashDao();
    private final OrderDao orderDao = new OrderDao();
    private final DeliveryDao deliveryDao = new DeliveryDao();


    public static SimpleDaoFactory getDaoFactory() {
        return factory;
    }

    @Override
    public UserDao getUserDao() {
        return userdao;
    }

    @Override
    public CarDao getCarDao() {
        return cardao;
    }

    @Override
    public CrashDao getCrashDao() {
        return crashDao;
    }

    @Override
    public OrderDao getOrderDao() {
        return orderDao;
    }

    @Override
    public DeliveryDao getDeliveryDao() {
        return deliveryDao;
    }
}
