package com.epam.rd.november.dao;

import com.epam.rd.november.models.Entity;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstract data access object class with basic CRUD operations.
 * @param <T> entity type
 */
public abstract class Dao<T extends Entity> {

    private final Logger logger = Logger.getLogger(Dao.class);
    private ConnectionPool pool = ConnectionPool.getConnectionPool();

    /**
     * Method that sets parameters to prepared statement.
     * @param entity entity object
     * @param prst preparedStatement object
     * @param isUpdate if there is update method - true, else false
     * @throws SQLException
     */
    public abstract void setParams(T entity, PreparedStatement prst, boolean isUpdate) throws SQLException;

    /**
     * Method that create entity object from result set.
     * @param resultSet
     * @return entity object
     * @throws SQLException
     */
    public abstract T getResult(ResultSet resultSet) throws SQLException;

    protected boolean create(T entity, String createQuery) {
        boolean result = true;
        try (Connection connection = pool.getConnection();
             PreparedStatement prst = connection.prepareStatement(createQuery, PreparedStatement.RETURN_GENERATED_KEYS)) {
            setParams(entity, prst, false);
            if (!(prst.executeUpdate() > 0)) {
                throw new SQLException(String.format("Error during creating object: %s", entity));
            }
            try (ResultSet generatedKeys = prst.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    entity.setId(generatedKeys.getInt(1));
                    logger.info(String.format("Object created successful: %s", entity));
                }
            }
        } catch (SQLException e) {
            logger.error("Create object failed.", e);
            result = false;
        }
        return result;
    }

    public T read(int id, String readQuery) {
        T entity = null;
        try (Connection connection = pool.getConnection();
             PreparedStatement prst = connection.prepareStatement(readQuery)) {
            prst.setInt(1, id);
            try (ResultSet rs = prst.executeQuery()) {
                rs.next();
                entity = getResult(rs);
                logger.info(String.format("Reading object with id: %d%n successful.", id));
            }
        } catch (SQLException e) {
            logger.error("Reading object failure.", e);
        }
        return entity;
    }

    public boolean update(T entity, String updateQuery) {
        boolean result = true;
        try (Connection connection = pool.getConnection();
             PreparedStatement prst = connection.prepareStatement(updateQuery)) {
            setParams(entity, prst, true);
            if (!(prst.executeUpdate() > 0)) {
                throw new SQLException(String.format("Error during updating object: %s", entity));
            }
            logger.info(String.format("Updating object: %s successful.", entity));
        } catch (SQLException e) {
            logger.error("Update object failed.", e);
            result = false;
        }
        return result;
    }

    public boolean delete(int id, String deleteQuery) {
        boolean result = true;
        try (Connection connection = pool.getConnection();
             PreparedStatement prst = connection.prepareStatement(deleteQuery)) {
            prst.setInt(1, id);
            if (!(prst.executeUpdate() > 0)) {
                throw new SQLException(String.format("Error during deleting object with id: %d%n", id));
            }
            logger.info(String.format("Deleting object with id: %d%n successful.", id));
        } catch (SQLException e) {
            logger.error("Deleting object failed.", e);
            result = false;
        }
        return result;
    }

    public List<T> getAll(String getAllQuery) {
        List<T> entitylist = new ArrayList<>();
        try (Connection connection = pool.getConnection();
             Statement st = connection.createStatement();
             ResultSet rs = st.executeQuery(getAllQuery)) {
            while (rs.next()) {
                T entity = getResult(rs);
                entitylist.add(entity);
            }
        } catch (SQLException e) {
            logger.error("Error during creating entity list.", e);
            e.printStackTrace();
        }
        return entitylist;
    }

    public int getCount(String countQuery) {
        int result = 0;
        try (Connection connection = pool.getConnection();
             PreparedStatement prst = connection.prepareStatement(countQuery)) {
            try (ResultSet rs = prst.executeQuery()) {
                rs.next();
                result = rs.getInt(1);
            }
        } catch (SQLException e) {
            logger.error("Error during counting objects.", e);
        }
        return result;
    }
}
