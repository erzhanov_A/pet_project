package com.epam.rd.november.dao.impl;

import com.epam.rd.november.dao.ConnectionPool;
import com.epam.rd.november.dao.Dao;
import com.epam.rd.november.models.Crash;
import com.epam.rd.november.models.Order;
import com.epam.rd.november.models.builders.OrderBuilder;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Dao class for Order entity.
 */
public class OrderDao extends Dao<Order> {
    private final Logger logger = Logger.getLogger(UserDao.class);
    private ConnectionPool pool = ConnectionPool.getConnectionPool();

    @Override
    public void setParams(Order entity, PreparedStatement prst, boolean isUpdate) throws SQLException {
        prst.setInt(1, entity.getUser().getId());
        prst.setInt(2, entity.getCar().getId());
        prst.setInt(3, entity.getAddress().getId());
        prst.setString(4, entity.getDescription());
        prst.setTimestamp(5, new Timestamp(entity.getFirstDay().getTime()));
        prst.setTimestamp(6, new Timestamp(entity.getLastDay().getTime()));
        prst.setBoolean(7, entity.isWithDriver());
        prst.setInt(8, entity.getBill());
        prst.setString(9, entity.getStatus().name());
        if (isUpdate) {
            prst.setInt(10, entity.getId());
        }
    }

    @Override
    public Order getResult(ResultSet resultSet) throws SQLException {
        return new OrderBuilder()
                .id(resultSet.getInt("oId"))
                .user(SimpleDaoFactory.getDaoFactory().getUserDao().read(resultSet.getInt("userId")))
                .car(SimpleDaoFactory.getDaoFactory().getCarDao().read(resultSet.getInt("carId")))
                .address(SimpleDaoFactory.getDaoFactory().getDeliveryDao().read(resultSet.getInt("addressId")))
                .crash(SimpleDaoFactory.getDaoFactory().getCrashDao().read(resultSet.getInt("crashId")))
                .description(resultSet.getString("description"))
                .firstDay(resultSet.getTimestamp("firstDay"))
                .lastDay(resultSet.getTimestamp("lastDay"))
                .withDriver(resultSet.getBoolean("isWithDriver"))
                .bill(resultSet.getInt("bill"))
                .status(Order.Status.valueOf(resultSet.getString("status")))
                .created(resultSet.getTimestamp("created"))
                .createOrder();
    }

    public List<Order> getRangeOfOrders(int startIndex, int count) {
        List<Order> orderList = new ArrayList<>();
        try (Connection connection = pool.getConnection();
             PreparedStatement prst = connection.prepareStatement("SELECT * FROM orders LIMIT ?, ?")) {
            prst.setInt(1, startIndex);
            prst.setInt(2, count);
            try (ResultSet rs = prst.executeQuery()) {
                while (rs.next()) {
                    Order entity = getResult(rs);
                    orderList.add(entity);
                }
            }
        } catch (SQLException e) {
            logger.error("Error during reading getting range of orders.", e);
            e.printStackTrace();
        }
        return orderList;
    }

    public List<Order> getAllUserOrders(int id) {
        logger.info(String.format("Getting orders with id %d%n ", id));
        List<Order> orders = new ArrayList<>();
        try (Connection connection = pool.getConnection();
             PreparedStatement prst = connection.prepareStatement("SELECT * FROM orders WHERE userId = ?")) {
            prst.setInt(1, id);
            try (ResultSet rs = prst.executeQuery()) {
                while (rs.next()) {
                    Order order = getResult(rs);
                    orders.add(order);
                }
            }
        } catch (SQLException ex) {
            logger.info(String.format("Getting orders list with id=%d%n  failure", id), ex);
        }
        return orders;
    }

    public boolean addCrash(Crash crash, Order order) {
        boolean result = true;
        String addCrash = "UPDATE orders SET crashId = ? WHERE oId = ?";
        try (Connection connection = pool.getConnection();
             PreparedStatement prst = connection.prepareStatement(addCrash)) {
            prst.setInt(1, crash.getId());
            prst.setInt(2, order.getId());
            if (!(prst.executeUpdate() > 0)) {
                throw new SQLException(String.format("Error during setting crash to order with id: %d%n", order.getId()));
            }
            logger.info(String.format("Setting crash to order with id %d%n  successful.", order.getId()));
        } catch (SQLException e) {
            logger.error("Add crash to order failure.", e);
            result = false;
        }
        return result;
    }

    public boolean updateStatus(Order order) {
        boolean result = true;
        String addCrash = "UPDATE orders SET status = ?, description = ? WHERE oId = ?";
        try (Connection connection = pool.getConnection();
             PreparedStatement prst = connection.prepareStatement(addCrash)) {
            prst.setString(1, String.valueOf(order.getStatus()));
            prst.setString(2, order.getDescription());
            prst.setInt(3, order.getId());
            if (!(prst.executeUpdate() > 0)) {
                throw new SQLException(String.format("Error during updating order status with id: %d%n", order.getId()));
            }
            logger.info(String.format("Updating order status with id %d%n  successful.", order.getId()));
        } catch (SQLException e) {
            logger.error("Update order status failure.", e);
            result = false;
        }
        return result;
    }

    public boolean create(Order entity) {
        String createQuery = "INSERT INTO orders (userId, carId, addressId, description, firstDay, " +
                "lastDay, isWithDriver, bill, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        return super.create(entity, createQuery);
    }

    public Order read(int id) {
        String readQuery = "SELECT * FROM orders WHERE oId = ?";
        return super.read(id, readQuery);
    }

    public boolean update(Order entity) {
        String updateQuery = "UPDATE orders SET userId = ?, carId = ?, addressId = ?, description = ?, " +
                "firstDay = ?,lastDay = ?, isWithDriver = ?, bill = ?, status = ? WHERE oId = ?";
        return super.update(entity, updateQuery);
    }

    public boolean delete(int id) {
        String deleteQuery = "DELETE FROM orders WHERE oId=?";
        return super.delete(id, deleteQuery);
    }

    public List<Order> getAll() {
        String getAllQuery = "SELECT * FROM orders";
        return super.getAll(getAllQuery);
    }

    public int getCount() {
        String countQuery = "SELECT COUNT(*) FROM orders";
        return super.getCount(countQuery);
    }

    public void deleteAllOrders() {
        try (Connection connection = pool.getConnection();
             Statement st = connection.createStatement()) {
            st.executeUpdate("DELETE FROM orders");
        } catch (SQLException ex) {
            logger.error("Deleting orders failed.", ex);
        }
    }
}
