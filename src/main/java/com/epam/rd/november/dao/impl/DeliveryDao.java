package com.epam.rd.november.dao.impl;

import com.epam.rd.november.dao.Dao;
import com.epam.rd.november.models.Delivery;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Dao class for Delivery entity.
 */
public class DeliveryDao extends Dao<Delivery> {
    @Override
    public void setParams(Delivery entity, PreparedStatement prst, boolean isUpdate) throws SQLException {
        prst.setString(1, entity.getCity());
        prst.setString(2, entity.getAddress());
        if (isUpdate) {
            prst.setInt(3, entity.getId());
        }
    }

    @Override
    public Delivery getResult(ResultSet resultSet) throws SQLException {
        Delivery del = new Delivery();
        del.setId(resultSet.getInt("dId"));
        del.setCity(resultSet.getString("city"));
        del.setAddress(resultSet.getString("address"));
        return del;
    }

    public boolean create(Delivery entity) {
        String createQuery = "INSERT INTO delivery (city, address) VALUES (?, ?)";
        return super.create(entity, createQuery);
    }

    public Delivery read(int id) {
        String readQuery = "SELECT * FROM delivery WHERE dId = ?";
        return super.read(id, readQuery);
    }

    public boolean update(Delivery entity) {
        String updateQuery = "UPDATE delivery SET city = ?, address = ? WHERE dId = ?";
        return super.update(entity, updateQuery);
    }

    public boolean delete(int id) {
        String deleteQuery = "DELETE FROM delivery WHERE dId=?";
        return super.delete(id, deleteQuery);
    }

    public List<Delivery> getAll() {
        String getAllQuery = "SELECT * FROM delivery";
        return super.getAll(getAllQuery);
    }

    public int getCount() {
        String countQuery = "SELECT COUNT(*) FROM delivery";
        return super.getCount(countQuery);
    }
}
