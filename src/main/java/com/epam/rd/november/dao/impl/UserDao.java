package com.epam.rd.november.dao.impl;

import com.epam.rd.november.dao.ConnectionPool;
import com.epam.rd.november.dao.Dao;
import com.epam.rd.november.models.User;
import com.epam.rd.november.models.builders.UserBuilder;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Dao class for User entity.
 */
public class UserDao extends Dao<User> {
    private final Logger logger = Logger.getLogger(UserDao.class);
    private ConnectionPool pool = ConnectionPool.getConnectionPool();

    public UserDao() {
    }

    public User getByLogin(String login) {
        User user = null;
        String query = "SELECT * FROM users WHERE login = ?";
        try (Connection connection = pool.getConnection();
             PreparedStatement prst = connection.prepareStatement(query)) {
            prst.setString(1, login);
            try (ResultSet rs = prst.executeQuery()) {
                rs.next();
                user = getResult(rs);
                logger.info(String.format("Reading user with login: %s successful.", login));
            }
        } catch (SQLException e) {
            logger.error("Reading user failure.", e);
        }
        return user;
    }

    public boolean changeUserStatus(int userId, User.UserStatus status) {
        boolean result = true;
        String addAdmin = "UPDATE users SET userStatus = ? WHERE uId = ?";
        try (Connection connection = pool.getConnection();
             PreparedStatement prst = connection.prepareStatement(addAdmin)) {
            prst.setString(1, String.valueOf(status));
            prst.setInt(2, userId);
            if (!(prst.executeUpdate() > 0)) {
                throw new SQLException(String.format("Error during updating user status with id %d%n ", userId));
            }
            logger.info(String.format("Updating user status with id %d%n successful.", userId));
        } catch (SQLException e) {
            logger.error("Changing status  failure.", e);
            result = false;
        }
        return result;
    }

    public boolean addDocument(String document, String login) {
        boolean result = true;
        String addDoc = "UPDATE users SET document = ? WHERE login = ?";
        try (Connection connection = pool.getConnection();
             PreparedStatement prst = connection.prepareStatement(addDoc)) {
            prst.setString(1, document);
            prst.setString(2, login);
            if (!(prst.executeUpdate() > 0)) {
                throw new SQLException(String.format("Error during updating user document with login: %s", login));
            }
            logger.info(String.format("Updating user document with login &s successful.", login));
        } catch (SQLException e) {
            logger.error("Add document failure.", e);
            result = false;
        }
        return result;
    }

    @Override
    public void setParams(User entity, PreparedStatement prst, boolean isUpdate) throws SQLException {
        prst.setString(1, entity.getFirstName());
        prst.setString(2, entity.getLastName());
        prst.setString(3, entity.getLogin());
        prst.setString(4, entity.getEmail());
        prst.setString(5, entity.getPassword());
        if (isUpdate) {
            prst.setInt(6, entity.getId());
        } else {
            User.Role role;
            if (entity.getRole() == null) {
                role = User.Role.CLIENT;
            } else {
                role = entity.getRole();
            }
            prst.setString(6, role.name());
            entity.setRole(User.Role.CLIENT);
            prst.setString(7, User.UserStatus.Active.name());
            entity.setUserStatus(User.UserStatus.Active);
        }
    }

    @Override
    public User getResult(ResultSet resultSet) throws SQLException {
        return new UserBuilder()
                .id(resultSet.getInt("uId"))
                .firstName(resultSet.getString("firstname"))
                .lastName(resultSet.getString("lastname"))
                .login(resultSet.getString("login"))
                .document(resultSet.getString("document"))
                .email(resultSet.getString("email"))
                .password(resultSet.getString("password"))
                .role(User.Role.valueOf(resultSet.getString("role")))
                .userStatus(User.UserStatus.valueOf(resultSet.getString("userStatus")))
                .createUser();
    }

    public boolean create(User entity) {
        String createQuery = "INSERT INTO users (firstname, lastname,  login, email, password, role, userStatus) VALUES (?, ?, ?, ?, ?, ?, ?)";
        return create(entity, createQuery);
    }

    public User read(int id) {
        String readQuery = "SELECT * FROM users WHERE uId = ?";
        return read(id, readQuery);
    }

    public boolean update(User entity) {
        String updateQuery = "UPDATE users SET firstname = ?, lastname = ?, login = ?, email = ?, password = ? WHERE uId = ?";
        return update(entity, updateQuery);
    }

    public boolean delete(int id) {
        String deleteQuery = "DELETE FROM users WHERE uId=?";
        return delete(id, deleteQuery);
    }

    public List<User> getAll() {
        String getAllQuery = "SELECT * FROM users";
        return getAll(getAllQuery);
    }

    public int getCount() {
        String countQuery = "SELECT COUNT(*) FROM users";
        return getCount(countQuery);
    }

    public List<User> getRangeOfUsers(int startIndex, int count) {
        List<User> entitylist = new ArrayList<>();
        try (Connection connection = pool.getConnection();
             PreparedStatement prst = connection.prepareStatement("SELECT * FROM users LIMIT ?, ?")) {
            prst.setInt(1, startIndex);
            prst.setInt(2, count);
            try (ResultSet rs = prst.executeQuery()) {
                while (rs.next()) {
                    User entity = getResult(rs);
                    entitylist.add(entity);
                }
            }
        } catch (SQLException ex) {
            logger.error("Error during creating entity list.", ex);
        }
        return entitylist;
    }

    public void deleteAllUsers() {
        try (Connection connection = pool.getConnection();
             Statement st = connection.createStatement()) {
            st.executeUpdate("DELETE FROM users");
        } catch (SQLException ex) {
            logger.error("Deleting users failed.", ex) ;
        }
    }
}
