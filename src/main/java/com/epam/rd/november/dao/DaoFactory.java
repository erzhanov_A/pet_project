package com.epam.rd.november.dao;

import com.epam.rd.november.dao.impl.*;

public interface DaoFactory {
    UserDao getUserDao();

    CarDao getCarDao();

    CrashDao getCrashDao();

    OrderDao getOrderDao();

    DeliveryDao getDeliveryDao();
}
