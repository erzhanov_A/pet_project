package com.epam.rd.november.dao.impl;

import com.epam.rd.november.dao.ConnectionPool;
import com.epam.rd.november.dao.Dao;
import com.epam.rd.november.models.Car;
import com.epam.rd.november.models.builders.CarBuilder;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Dao class for Car entity.
 */
public class CarDao extends Dao<Car> {
    private final Logger logger = Logger.getLogger(CarDao.class);
    private ConnectionPool pool = ConnectionPool.getConnectionPool();


    @Override
    public void setParams(Car entity, PreparedStatement prst, boolean isUpdate) throws SQLException {
        prst.setString(1, entity.getMake());
        prst.setString(2, entity.getModel());
        prst.setString(3, entity.getType().toString());
        prst.setInt(4, entity.getPricePerDay());
        if (isUpdate) {
            prst.setInt(5, entity.getId());
        }
    }

    @Override
    public Car getResult(ResultSet resultSet) throws SQLException {
        return new CarBuilder()
                .id(resultSet.getInt("cId"))
                .make(resultSet.getString("make"))
                .model(resultSet.getString("model"))
                .type(Car.CarType.valueOf(resultSet.getString("type")))
                .pricePerDay(resultSet.getInt("pricePerDay"))
                .createCar();
    }


    public List<Car> getRangeOfCars(int startIndex, int count) {
        List<Car> entitylist = new ArrayList<>();
        try (Connection connection = pool.getConnection();
             PreparedStatement prst = connection.prepareStatement("SELECT * FROM cars LIMIT ?, ?")) {
            prst.setInt(1, startIndex);
            prst.setInt(2, count);
            try (ResultSet rs = prst.executeQuery()) {
                while (rs.next()) {
                    Car entity = getResult(rs);
                    entitylist.add(entity);
                }
            }
        } catch (SQLException e) {
            logger.error("Error during creating entity list.", e);
            e.printStackTrace();
        }
        return entitylist;
    }

    public List<Car> getAllByOptionValue(String option, String value) {
        logger.info(String.format("Getting cars with value %s ", value));
        List<Car> carList = new ArrayList<>();
        try (Connection connection = pool.getConnection();
             PreparedStatement prst = connection.prepareStatement(String.format("SELECT * FROM cars WHERE %s = ?", option))) {
            prst.setString(1, value);
            try (ResultSet rs = prst.executeQuery()) {
                while (rs.next()) {
                    Car car = getResult(rs);
                    carList.add(car);
                }
            }
        } catch (SQLException ex) {
            logger.info(String.format("Getting cars with value %s failure", value), ex);
        }
        return carList;
    }

    public List<String> getCarOption(String carOption) {
        logger.info(String.format("Getting car options %s from cars table ", carOption));
        List<String> carOptionsList = new ArrayList<>();
        try (Connection connection = pool.getConnection();
             Statement st = connection.createStatement();
             ResultSet rs = st.executeQuery(String.format("SELECT DISTINCT %s FROM cars", carOption))) {
            while (rs.next()) {
                String option = rs.getString(carOption);
                carOptionsList.add(option);
            }
        } catch (SQLException ex) {
            logger.info(String.format("Getting car options %s from cars table failure", carOption), ex);
        }
        return carOptionsList;
    }

    public boolean create(Car entity) {
        String createQuery = "INSERT INTO cars (make, model, type, pricePerDay) VALUES (?, ?, ?, ?)";
        return super.create(entity, createQuery);
    }

    public Car read(int id) {
        String readQuery = "SELECT * FROM cars WHERE cId = ?";
        return super.read(id, readQuery);
    }

    public boolean update(Car entity) {
        String updateQuery = "UPDATE cars SET make = ?, model = ?, type = ?, pricePerDay = ? WHERE cId = ?";
        return super.update(entity, updateQuery);
    }

    public boolean delete(int id) {
        String deleteQuery = "DELETE FROM cars WHERE cId=?";
        return super.delete(id, deleteQuery);
    }

    public List<Car> getAll() {
        String getAllQuery = "SELECT * FROM cars";
        return super.getAll(getAllQuery);
    }

    public int getCount() {
        String countQuery = "SELECT COUNT(*) FROM cars";
        return super.getCount(countQuery);
    }
}
