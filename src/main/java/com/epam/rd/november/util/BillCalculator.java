package com.epam.rd.november.util;

import java.util.Date;

/**
 * The class for order calculation.
 */
public class BillCalculator {

    public static int getOrderSumm(Date firstDay, Date lastDay, int pricePerDay) {
        long diff = Math.abs(lastDay.getTime() - firstDay.getTime());
        long diffDays = diff / (24 * 60 * 60 * 1000);
        int res = Math.toIntExact(diffDays * pricePerDay);
        return res;
    }
}
