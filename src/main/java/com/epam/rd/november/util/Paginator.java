package com.epam.rd.november.util;

import javax.servlet.http.HttpServletRequest;

/**
 * The class for pagination objects at web app.
 */
public class Paginator {

    public static int getCurrentPage(HttpServletRequest req) {
        int page = 1;
        if (req.getParameter("page") != null) {
            page = Integer.parseInt(req.getParameter("page"));
        }
        return page;
    }

    public static int getCountOfPages(int countOfRecords, int recordsPerPage) {
        int countOfPages = (int) Math.ceil(countOfRecords * 1.0 / recordsPerPage);
        return countOfPages;
    }
}
