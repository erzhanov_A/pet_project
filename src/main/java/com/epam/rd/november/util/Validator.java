package com.epam.rd.november.util;


import org.apache.log4j.Logger;

/**
 * The class for validation input params.
 */
public class Validator {
    private static final Logger logger = Logger.getLogger(Validator.class);

    private static final String loginReg = "^[a-z0-9]{3,20}$";
    private static final String emailReg = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String pass = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{5,20})";
    private static final String firstname = "([A-Z][a-zA-Z]+)|([А-Я][а-я]+)";
    private static final String lastname = "([A-Z][a-zA-Z]+)|([А-Я][а-я]+)";
    private static final String documentReg = "([A-Z]+\\s\\d{3,10})|([А-Я]+\\s\\d{3,10})";
    private static final String addressReg = "([A-z]{4,10})+(\\s\\d{1,4})(\\/\\d{1,4})*|([А-я]{4,10})+(\\s\\d{1,4})(\\/\\d{1,4})*";
    private static final String cityReg = "^([A-z]{4,15})|([А-я]{4,15})";


    private Validator validator = null;

    private Validator() {
    }

    public static Validator getInstance() {
        return new Validator();
    }

    public boolean isAddressValid(String address) {
        if (address == null || address.isEmpty()) {
            logger.info("Address is empty");
            return false;
        } else {
            logger.info(String.format("Address is valid: %b", address.matches(addressReg)));
            return address.matches(addressReg);
        }
    }

    public boolean isDocmentValid(String document) {
        if (document == null || document.isEmpty()) {
            logger.info("Document is empty");
            return false;
        } else {
            logger.info(String.format("Document is valid: %b", document.matches(documentReg)));
            return document.matches(documentReg);
        }
    }

    public boolean isCityValid(String city) {
        if (city == null || city.isEmpty()) {
            logger.info("City is empty");
            return false;
        } else {
            logger.info(String.format("City is valid: %b", city.matches(cityReg)));
            return city.matches(cityReg);
        }
    }

    public boolean isloginValid(String login) {
        if (login == null || login.isEmpty()) {
            logger.info("User login is empty");
            return false;
        } else {
            logger.info(String.format("User login is valid: %b", login.matches(loginReg)));
            return login.matches(loginReg);
        }
    }

    public boolean isEmailValid(String email) {
        if (email == null || email.isEmpty()) {
            logger.info("User email is empty");
            return false;
        } else {
            logger.info(String.format("User email is valid: %b", email.matches(emailReg)));
            return email.matches(emailReg);
        }
    }

    public boolean isPasswordValid(String password) {
        if (password == null || password.isEmpty()) {
            logger.info("User password is empty");
            return false;
        } else {
            logger.info(String.format("User password is valid: %b", password.matches(pass)));
            return password.matches(pass);
        }
    }

    public boolean isFirstNameValid(String firstName) {
        if (firstName == null || firstName.isEmpty()) {
            logger.info("User first name is empty");
            return false;
        } else {
            logger.info(String.format("User first name is valid: %b", firstName.matches(firstname)));
            return firstName.matches(firstname);
        }
    }

    public boolean isLastNameValid(String lastName) {
        if (lastName == null || lastName.isEmpty()) {
            logger.info("User last name is empty");

            return false;
        } else {
            logger.info(String.format("User last name is valid: %b", lastName.matches(lastname)));
            return lastName.matches(lastname);
        }
    }
}