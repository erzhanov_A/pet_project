package com.epam.rd.november.comands.orders;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.resource.ConfigManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command that return update order status page.
 */
public class GetUpdateOrderStatusCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(GetUpdateOrderStatusCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        String orderId = req.getParameter("updateStatusId");
        if (session.getAttribute("role").equals("MANAGER") && orderId != null) {
            req.getSession().setAttribute("updateStatusId", orderId);
            req.getSession().setAttribute("desk", "Description");
            logger.info("Redirect to update status command");
            return ConfigManager.getProperty("path.page.update.status");
        } else {
            return ConfigManager.getProperty("path.page.index");
        }
    }
}
