package com.epam.rd.november.comands.user;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.models.User;
import com.epam.rd.november.resource.ConfigManager;
import com.epam.rd.november.resource.MessageManager;
import com.epam.rd.november.services.impl.SimpleServiceFactory;
import com.epam.rd.november.util.HashPassword;
import com.epam.rd.november.util.Validator;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command that provides the login to the web app.
 */
public class LoginCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(LoginCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        String page;
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        String validResult = validateForm(login, password);
        if (!validResult.equals("valid")) {
            req.setAttribute("errorMessage", validResult);
            logger.info("Params validation failure.");
            page = ConfigManager.getProperty("path.page.login");
            return page;
        }

        User user = SimpleServiceFactory.getServiceFactory()
                .getUserService()
                .getByLogin(login);
        if (user == null) {
            logger.info("User not found.");
            req.setAttribute("errorMessage", MessageManager.getProperty("message.login.usernotfound"));
            return ConfigManager.getProperty("path.page.login");
        }

        if (!HashPassword.checkPassword(password, user.getPassword())) {
            logger.info("Authentication failed. Incorrect password.");
            req.setAttribute("errorMessage", MessageManager.getProperty("message.login.error"));
            return ConfigManager.getProperty("path.page.login");
        }

        if (user.getUserStatus().equals(User.UserStatus.Blocked)) {
            logger.info("Access denied, user is blocked!");
            req.setAttribute("errorMessage", MessageManager.getProperty("message.user.blocked"));
            return ConfigManager.getProperty("path.page.login");
        }

        logger.info("Authorisation success.");
        HttpSession session = req.getSession();
        setUserInfo(user, session);
        if (user.getRole().equals(User.Role.ADMIN)) {
            page = ConfigManager.getProperty("path.page.admin");
        } else if (user.getRole().equals(User.Role.MANAGER)) {
            page = ConfigManager.getProperty("path.page.manager");
        } else {
            page = ConfigManager.getProperty("path.page.user");
        }
        return page;
    }

    /**
     * Validation user information.
     * @param email user email
     * @param password user password
     * @return if all params are valid - return string "valid", else error message
     */
    private String validateForm(String email, String password) {
        Validator validator = Validator.getInstance();
        if (!validator.isPasswordValid(password)) {
            return MessageManager.getProperty("message.invalid.password");
        }
        if (!validator.isloginValid(email)) {
            return MessageManager.getProperty("message.invalid.login");
        }
        return "valid";
    }


    /**
     * Add user info to the session object.
     * @param user user object
     * @param session session object
     */
    private void setUserInfo(User user, HttpSession session) {
        session.setAttribute("firstname", user.getFirstName());
        session.setAttribute("lastname", user.getLastName());
        session.setAttribute("userId", user.getId());
        session.setAttribute("role", user.getRole().toString());
    }
}
