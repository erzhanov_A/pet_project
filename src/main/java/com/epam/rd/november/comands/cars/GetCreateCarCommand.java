package com.epam.rd.november.comands.cars;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.resource.ConfigManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command that return create car page.
 */
public class GetCreateCarCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(GetCreateCarCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        if (session.getAttribute("role").equals("ADMIN")) {
            logger.info("Redirect to create car command");
            return ConfigManager.getProperty("path.page.newcar");
        } else {
            return ConfigManager.getProperty("path.page.index");
        }
    }
}
