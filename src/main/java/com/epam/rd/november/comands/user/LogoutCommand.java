package com.epam.rd.november.comands.user;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.resource.ConfigManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command that provides the log out from the web app
 */
public class LogoutCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(LogoutCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        if (session != null && session.getAttribute("userId") != null) {
            session.removeAttribute("role");
            session.removeAttribute("userId");
            session.removeAttribute("firstname");
            session.removeAttribute("lastname");
            logger.info("User logout successful");
        }
        return ConfigManager.getProperty("path.page.index");
    }
}
