package com.epam.rd.november.comands.cars;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.models.Car;
import com.epam.rd.november.resource.ConfigManager;
import com.epam.rd.november.resource.MessageManager;
import com.epam.rd.november.services.impl.SimpleServiceFactory;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static com.epam.rd.november.comands.cars.CreateCarCommand.createCarFromRequest;

/**
 * The command that provides updating car in the web app.
 */
public class UpdateCarCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(UpdateCarCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        String page;
        HttpSession session = req.getSession(false);
        Integer carId = Integer.valueOf(req.getParameter("updateCarId"));
        if (session.getAttribute("role").equals("ADMIN")) {
            Car car = createCarFromRequest(req);
            car.setId(carId);
            boolean updateRes = SimpleServiceFactory.getServiceFactory()
                    .getCarService()
                    .update(car);
            if (!updateRes) {
                req.setAttribute("errorMessage", MessageManager.getProperty("message.car.updating.failure"));
                logger.info("Insert car to database failed");
                page = ConfigManager.getProperty("path.page.dberror");
                return page;
            }
            page = ConfigManager.getProperty("path.page.admin");
            logger.info("Car updating successful.");
            return page;

        } else {
            return ConfigManager.getProperty("path.page.index");
        }
    }
}
