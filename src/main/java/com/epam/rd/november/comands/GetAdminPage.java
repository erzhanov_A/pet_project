package com.epam.rd.november.comands;

import com.epam.rd.november.resource.ConfigManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command that return admin page.
 */
public class GetAdminPage implements ActionCommand {

    @Override
    public String execute(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        if (session.getAttribute("role").equals("ADMIN")) {
            return ConfigManager.getProperty("path.page.admin");
        } else {
            return ConfigManager.getProperty("path.page.index");
        }
    }
}
