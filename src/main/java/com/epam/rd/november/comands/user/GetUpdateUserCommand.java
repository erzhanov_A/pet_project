package com.epam.rd.november.comands.user;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.resource.ConfigManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


public class GetUpdateUserCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(GetUpdateUserCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        if (session.getAttribute("role").equals("CLIENT")) {
            logger.info("Redirect to update user command");
            return ConfigManager.getProperty("path.page.registration");
        } else {
            return ConfigManager.getProperty("path.page.index");
        }
    }
}
