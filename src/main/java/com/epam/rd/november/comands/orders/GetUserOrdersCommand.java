package com.epam.rd.november.comands.orders;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.models.Order;
import com.epam.rd.november.resource.ConfigManager;
import com.epam.rd.november.services.impl.SimpleServiceFactory;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * The command that return all user orders.
 */
public class GetUserOrdersCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(GetUserOrdersCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        if (session.getAttribute("role").equals("CLIENT") && session.getAttribute("userId") != null) {
            List<Order> userOrders = SimpleServiceFactory.getServiceFactory().getOrderService()
                    .getUserOrders((Integer) session.getAttribute("userId"));
            session.setAttribute("userOrders", userOrders);
            logger.info("Redirect to user orders");
            return ConfigManager.getProperty("path.page.user.orders");
        } else {
            return ConfigManager.getProperty("path.page.index");
        }
    }
}
