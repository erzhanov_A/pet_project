package com.epam.rd.november.comands.user;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.resource.ConfigManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command that return delete user page.
 */
public class GetDeleteUserCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(GetDeleteUserCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        if (session.getAttribute("role").equals("CLIENT")) {
            logger.info("Redirect to delete user command");
            return ConfigManager.getProperty("path.page.delete.user");
        } else {
            return ConfigManager.getProperty("path.page.index");
        }
    }
}
