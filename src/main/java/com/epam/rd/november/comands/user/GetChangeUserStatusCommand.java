package com.epam.rd.november.comands.user;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.resource.ConfigManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command that return change user status page.
 */
public class GetChangeUserStatusCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(GetChangeUserStatusCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        String userId = req.getParameter("user");
        if (session.getAttribute("role").equals("ADMIN") && userId != null) {
            req.getSession().setAttribute("blockUser", userId);
            logger.info("Redirect to change status command");
            return ConfigManager.getProperty("path.page.block");
        } else {
            return ConfigManager.getProperty("path.page.index");
        }
    }
}
