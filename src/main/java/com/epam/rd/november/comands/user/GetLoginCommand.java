package com.epam.rd.november.comands.user;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.resource.ConfigManager;
import org.apache.log4j.Logger;


import javax.servlet.http.HttpServletRequest;

/**
 * The command that return  page.
 */
public class GetLoginCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(GetLoginCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        String page;
        logger.info("Redirect to login page");
        page = ConfigManager.getProperty("path.page.login");
        return page;
    }
}
