package com.epam.rd.november.comands;

import com.epam.rd.november.resource.ConfigManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command that return manager page.
 */
public class GetManagerPage implements ActionCommand {

    @Override
    public String execute(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        if (session.getAttribute("role").equals("MANAGER")) {
            return ConfigManager.getProperty("path.page.manager");
        } else {
            return ConfigManager.getProperty("path.page.index");
        }
    }
}
