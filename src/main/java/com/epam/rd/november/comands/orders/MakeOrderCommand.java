package com.epam.rd.november.comands.orders;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.models.Car;
import com.epam.rd.november.models.Delivery;
import com.epam.rd.november.models.Order;
import com.epam.rd.november.models.User;
import com.epam.rd.november.models.builders.OrderBuilder;
import com.epam.rd.november.resource.ConfigManager;
import com.epam.rd.november.resource.MessageManager;
import com.epam.rd.november.services.impl.SimpleServiceFactory;
import com.epam.rd.november.util.BillCalculator;
import com.epam.rd.november.util.Validator;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The command that provides creating new order in the web app.
 */
public class MakeOrderCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(MakeOrderCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        String page;
        HttpSession session = req.getSession();
        Integer userId = (Integer) session.getAttribute("userId");
        if (userId == null) {
            logger.info("User is not login.");
            page = ConfigManager.getProperty("path.page.login");
            return page;
        }
        String city = req.getParameter("city");
        String address = req.getParameter("address");
        String document = req.getParameter("document");
        String validRes = validateParams(city, address, document);
        if (!validRes.equals("valid")) {
            req.setAttribute("errorMessage", validRes);
            logger.info("Order validation failure.");
            page = ConfigManager.getProperty("path.page.order");
            return page;
        }

        User user = SimpleServiceFactory.getServiceFactory().getUserService().read(userId);
        SimpleServiceFactory.getServiceFactory().getUserService().addDocument(document, user.getLogin());

        Integer carId = Integer.valueOf(req.getParameter("carId"));
        Car car = SimpleServiceFactory.getServiceFactory().getCarService().read(carId);

        Delivery delivery = new Delivery();
        delivery.setCity(city);
        delivery.setAddress(address);
        boolean delRes = SimpleServiceFactory.getServiceFactory().getDeliveryService().create(delivery);
        if (!delRes) {
            req.setAttribute("errorMessage", MessageManager.getProperty("message.delivery.creation.failure"));
            logger.info("Insert delivery to database failed");
            page = ConfigManager.getProperty("path.page.dberror");
            return page;
        }

        Date firstDate = parseDate(req.getParameter("firstdate"));
        Date lastDate = parseDate(req.getParameter("lastdate"));

        if ((firstDate == null || lastDate == null) || firstDate.compareTo(lastDate) >= 0) {
            req.setAttribute("errorMessage", MessageManager.getProperty("message.rent.time.invalid"));
            logger.info("Rent time is invalid");
            page = ConfigManager.getProperty("path.page.order");
            return page;
        }
        int billSum = BillCalculator.getOrderSumm(firstDate, lastDate, car.getPricePerDay());

        boolean withDriver = Boolean.parseBoolean(req.getParameter("isWithDriver"));
        if (withDriver) {
            int driverSalaryPerDay = Integer.parseInt(ConfigManager.getProperty("driver.salary"));
            int driverSalary = BillCalculator.getOrderSumm(firstDate, lastDate, driverSalaryPerDay);
            billSum += driverSalary;
        }

        Order newOrder = new OrderBuilder()
                .user(user)
                .car(car)
                .withDriver(withDriver)
                .status(Order.Status.CREATED)
                .bill(billSum)
                .firstDay(firstDate)
                .lastDay(lastDate)
                .description(req.getParameter("description"))
                .address(delivery)
                .createOrder();

        boolean createRes = SimpleServiceFactory.getServiceFactory().getOrderService().create(newOrder);

        if (!createRes) {
            req.setAttribute("errorMessage", MessageManager.getProperty("message.order.creation.failure"));
            logger.info("Insert order to database failed");
            page = ConfigManager.getProperty("path.page.dberror");
            return page;
        }
        page = ConfigManager.getProperty("path.page.user");
        logger.info("New order added to database.");
        session.setAttribute("orderId", newOrder.getId());
        return page;
    }

    /**
     * Create date object from string date.
     * @param strDate string date
     * @return date
     */
    private Date parseDate(String strDate) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            return format.parse(strDate);
        } catch (ParseException e) {
            logger.error(String.format("Error during parsing date %s", strDate));
            return null;
        }
    }

    /**
     * Validation order information.
     * @param city City
     * @param address Street and number of house
     * @param document Client document ID
     * @return if all params are valid - return string "valid", else error message
     */
    private String validateParams(String city, String address, String document) {
        Validator validator = Validator.getInstance();
        if (!validator.isCityValid(city)) {
            return MessageManager.getProperty("message.invalid.city");
        }
        if (!validator.isAddressValid(address)) {
            return MessageManager.getProperty("message.invalid.address");
        }
        if (!validator.isDocmentValid(document)) {
            return MessageManager.getProperty("message.invalid.document");
        }
        return "valid";
    }
}
