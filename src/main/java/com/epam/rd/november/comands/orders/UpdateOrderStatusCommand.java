package com.epam.rd.november.comands.orders;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.models.Order;
import com.epam.rd.november.resource.ConfigManager;
import com.epam.rd.november.resource.MessageManager;
import com.epam.rd.november.services.impl.SimpleServiceFactory;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command that provides updating order status.
 */
public class UpdateOrderStatusCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(UpdateOrderStatusCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        String page;
        HttpSession session = req.getSession(false);
        if (session.getAttribute("role").equals("MANAGER")) {
            Order order = createOrderFromRequest(req);
            boolean updateStatusRes = SimpleServiceFactory.getServiceFactory().getOrderService()
                    .updateStatus(order);
            if (!updateStatusRes) {
                req.setAttribute("errorMessage", MessageManager.getProperty("message.update.status.failure"));
                logger.info("Updating order status failed");
                page = ConfigManager.getProperty("path.page.dberror");
                return page;
            }
            page = ConfigManager.getProperty("path.page.manager");
            logger.info("Status was updated successful.");
            return page;

        } else {
            return ConfigManager.getProperty("path.page.index");
        }
    }

    private Order createOrderFromRequest(HttpServletRequest req) {
        Order order = new Order();
        order.setId(Integer.valueOf(req.getParameter("updateStatusId")));
        order.setStatus(Order.Status.valueOf(req.getParameter("status")));
        order.setDescription(req.getParameter("description"));
        return order;
    }
}
