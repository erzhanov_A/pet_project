package com.epam.rd.november.comands.cars;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.resource.ConfigManager;
import com.epam.rd.november.resource.MessageManager;
import com.epam.rd.november.services.impl.SimpleServiceFactory;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command that provides deleting car from the web app.
 */
public class DeleteCarCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(DeleteCarCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        String page;
        HttpSession session = req.getSession(false);
        if (session.getAttribute("role").equals("ADMIN")) {
            Integer carId = Integer.valueOf(req.getParameter("deleteCarId"));
            boolean deleteRes = SimpleServiceFactory.getServiceFactory()
                    .getCarService().delete(carId);
            if (!deleteRes) {
                req.setAttribute("errorMessage", MessageManager.getProperty("message.car.deleting.failure"));
                logger.info("Deleting car from database failed");
                page = ConfigManager.getProperty("path.page.dberror");
                return page;
            }
            page = ConfigManager.getProperty("path.page.admin");
            logger.info("Car deleted successful.");
            return page;
        } else {
            return ConfigManager.getProperty("path.page.index");
        }
    }
}
