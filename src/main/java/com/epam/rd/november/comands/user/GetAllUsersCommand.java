package com.epam.rd.november.comands.user;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.models.User;
import com.epam.rd.november.resource.ConfigManager;
import com.epam.rd.november.services.impl.SimpleServiceFactory;
import com.epam.rd.november.util.Paginator;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * The command that return all users in the web app.
 */
public class GetAllUsersCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(GetAllUsersCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        if (session.getAttribute("role").equals("ADMIN")) {
            int recordsPerPage = Integer.parseInt(ConfigManager.getProperty("users.per.page"));
            logger.info(String.format("Records per page set is %d%n.", recordsPerPage));
            int currentPage = Paginator.getCurrentPage(req);
            int countOfCars = SimpleServiceFactory.getServiceFactory()
                    .getOrderService().getCount();
            int numberOfPages = Paginator.getCountOfPages(countOfCars, recordsPerPage);

            List<User> userList = SimpleServiceFactory.getServiceFactory().getUserService()
                    .getRangeOfUsers((currentPage - 1) * recordsPerPage, recordsPerPage);

            session.setAttribute("users", userList);
            session.setAttribute("currentPage", currentPage);
            session.setAttribute("numberOfPages", numberOfPages);
            return ConfigManager.getProperty("path.page.users");
        } else {
            return ConfigManager.getProperty("path.page.index");
        }
    }
}
