package com.epam.rd.november.comands.orders;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.models.Crash;
import com.epam.rd.november.models.Order;
import com.epam.rd.november.resource.ConfigManager;
import com.epam.rd.november.resource.MessageManager;
import com.epam.rd.november.services.impl.SimpleServiceFactory;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command that provides creating a crash and add to the order.
 */
public class AddCrashCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(AddCrashCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        String page;
        HttpSession session = req.getSession(false);
        Integer orderId = Integer.valueOf(req.getParameter("orderId"));
        if (session.getAttribute("role").equals("MANAGER")) {
            Crash crash = createCrashFromRequest(req);
            boolean createRes = SimpleServiceFactory.getServiceFactory().getCrashService()
                    .create(crash);
            if (!createRes) {
                req.setAttribute("errorMessage", MessageManager.getProperty("message.crash.creating.failure"));
                logger.info("Insert crash to database failed");
                page = ConfigManager.getProperty("path.page.dberror");
                return page;
            }
            Order order = SimpleServiceFactory.getServiceFactory().getOrderService().read(orderId);
            boolean addCrashRes = SimpleServiceFactory.getServiceFactory().getOrderService()
                    .addCrash(crash, order);
            if (!addCrashRes) {
                req.setAttribute("errorMessage", MessageManager.getProperty("message.addcrash.failure"));
                logger.info("Setting crash to order failed");
                page = ConfigManager.getProperty("path.page.dberror");
                return page;
            }

            page = ConfigManager.getProperty("path.page.manager");
            logger.info("Crash creating successful.");
            return page;

        } else {
            return ConfigManager.getProperty("path.page.index");
        }
    }

    private Crash createCrashFromRequest(HttpServletRequest req) {
        Crash crash = new Crash();
        crash.setDamageSum(Integer.parseInt(req.getParameter("damageSum")));
        crash.setCrashDescription(req.getParameter("crashDescription"));
        crash.setIsPaid(Boolean.parseBoolean(req.getParameter("isPaid")));
        return crash;
    }
}
