package com.epam.rd.november.comands.user;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.models.User;
import com.epam.rd.november.models.builders.UserBuilder;
import com.epam.rd.november.resource.ConfigManager;
import com.epam.rd.november.resource.MessageManager;
import com.epam.rd.november.services.impl.SimpleServiceFactory;
import com.epam.rd.november.util.HashPassword;
import com.epam.rd.november.util.Validator;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command that provides registration in the web app.
 */
public class RegisterCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(RegisterCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        String page;
        User user = createUserFromRequest(req);
        String validResult = validateUser(user);
        if (!validResult.equals("valid")) {
            req.setAttribute("errorMessage", validResult);
            logger.info("User validation failure.");
            page = ConfigManager.getProperty("path.page.registration");
            return page;
        }
        user.setPassword(HashPassword.hashPassword(user.getPassword()));

        if (Boolean.valueOf(req.getParameter("isManager"))) {
            user.setRole(User.Role.MANAGER);
        }

        boolean createRes = SimpleServiceFactory.getServiceFactory()
                .getUserService()
                .create(user);
        if (!createRes) {
            req.setAttribute("errorMessage", MessageManager.getProperty("message.user.creation.failure"));
            logger.info("Insert user to database failed");
            page = ConfigManager.getProperty("path.page.dberror");
            return page;
        }
        page = ConfigManager.getProperty("path.page.success");
        logger.info("New user added to database.");
        HttpSession session = req.getSession();
        setUserInfo(user, session);
        return page;
    }

    /**
     * Validation user information.
     * @param user user object
     * @return if all params are valid - return string "valid", else error message
     */
    static String validateUser(User user) {
        Validator validator = Validator.getInstance();
        if (!validator.isPasswordValid(user.getPassword())) {
            return MessageManager.getProperty("message.invalid.password");
        }
        if (!validator.isEmailValid(user.getEmail())) {
            return MessageManager.getProperty("message.invalid.email");
        }
        if (!validator.isloginValid(user.getLogin())) {
            return MessageManager.getProperty("message.invalid.login");
        }
        if (!validator.isFirstNameValid(user.getFirstName())) {
            return MessageManager.getProperty("message.invalid.firstname");
        }
        if (!validator.isLastNameValid(user.getLastName())) {
            return MessageManager.getProperty("message.invalid.lastname");
        }
        return "valid";
    }

    /**
     * Add user info to the session object.
     * @param user user object
     * @param session session object
     */
    private void setUserInfo(User user, HttpSession session) {
        session.setAttribute("firstname", user.getFirstName());
        session.setAttribute("lastname", user.getLastName());
    }

    /**
     * Create user object with parameters from request.
     * @param req request
     * @return user
     */
    static User createUserFromRequest(HttpServletRequest req) {
        return new UserBuilder()
                .firstName(req.getParameter("firstname"))
                .lastName(req.getParameter("lastname"))
                .login(req.getParameter("login"))
                .email(req.getParameter("email"))
                .password(req.getParameter("password"))
                .createUser();
    }
}
