package com.epam.rd.november.comands.orders;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.resource.ConfigManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command that return add crash page.
 */
public class GetAddCrashCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(GetAddCrashCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        String orderId = req.getParameter("orderId");
        if (session.getAttribute("role").equals("MANAGER") && orderId != null) {
            req.getSession().setAttribute("orderId", orderId);
            req.getSession().setAttribute("desk", "Description");
            logger.info("Redirect to addCrash command");
            return ConfigManager.getProperty("path.page.addcrash");
        } else {
            return ConfigManager.getProperty("path.page.index");
        }
    }
}
