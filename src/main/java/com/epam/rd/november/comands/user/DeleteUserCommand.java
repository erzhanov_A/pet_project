package com.epam.rd.november.comands.user;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.resource.ConfigManager;
import com.epam.rd.november.resource.MessageManager;
import com.epam.rd.november.services.impl.SimpleServiceFactory;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command that provides deleting user from the web app.
 */
public class DeleteUserCommand implements ActionCommand {

    private final Logger logger = Logger.getLogger(DeleteUserCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        String page;
        HttpSession session = req.getSession(false);
        if (session.getAttribute("role").equals("CLIENT")) {
            Integer userId = (Integer) session.getAttribute("userId");
            boolean deleteRes = SimpleServiceFactory.getServiceFactory()
                    .getUserService().delete(userId);
            if (!deleteRes) {
                req.setAttribute("errorMessage", MessageManager.getProperty("message.user.deleting.failure"));
                logger.info("Deleting user from database failed");
                page = ConfigManager.getProperty("path.page.dberror");
                return page;
            }
            page = ConfigManager.getProperty("path.page.index");
            session.removeAttribute("role");
            session.removeAttribute("userId");
            logger.info("User deleted successful.");
            return page;
        } else {
            return ConfigManager.getProperty("path.page.index");
        }
    }
}
