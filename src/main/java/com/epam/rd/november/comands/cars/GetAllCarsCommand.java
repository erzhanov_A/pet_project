package com.epam.rd.november.comands.cars;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.models.Car;
import com.epam.rd.november.resource.ConfigManager;
import com.epam.rd.november.services.impl.SimpleServiceFactory;
import com.epam.rd.november.util.Paginator;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Comparator;
import java.util.List;

/**
 * The command that return all cars in the web app.
 */
public class GetAllCarsCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(GetAllCarsCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        if (session.getAttribute("role").equals("CLIENT") || session.getAttribute("role").equals("ADMIN")) {
            int recordsPerPage = Integer.parseInt(ConfigManager.getProperty("cars.per.page"));
            logger.info(String.format("Records per page set is %d%n.", recordsPerPage));
            int currentPage = Paginator.getCurrentPage(req);
            int countOfCars = SimpleServiceFactory.getServiceFactory()
                    .getCarService().getCount();
            int numberOfPages = Paginator.getCountOfPages(countOfCars, recordsPerPage);

            String type = req.getParameter("carType");
            String make = req.getParameter("carMake");

            List<String> types = SimpleServiceFactory.getServiceFactory().getCarService().getCarTypes();
            List<String> makes = SimpleServiceFactory.getServiceFactory().getCarService().getCarMakes();
            List<Car> cars = getCars(make, type, currentPage, recordsPerPage);

            String sortReq = req.getParameter("sort");

            if (sortReq != null && sortReq.equals("name")) {
                cars = sortByName(cars);
            }
            if (sortReq != null && sortReq.equals("price")) {
                cars = sortByPrice(cars);
            }

            session.setAttribute("cars", cars);
            session.setAttribute("carTypes", types);
            session.setAttribute("carMakes", makes);
            session.setAttribute("currentPage", currentPage);
            session.setAttribute("numberOfPages", numberOfPages);
            logger.info("Get all cars command finished.");
            return ConfigManager.getProperty("path.page.cars");
        } else {
            return ConfigManager.getProperty("path.page.index");
        }
    }

    /**
     * Sorting list cars by name.
     * @param cars list cars
     * @return sorted list cars
     */
    private List<Car> sortByName(List<Car> cars) {
        logger.info("Cars sorting by name.");
        Comparator<Car> nameComparator = Comparator.comparing(Car::getMake);
        cars.sort(nameComparator);
        return cars;
    }

    /**
     * Sorting list cars by price.
     * @param cars list cars
     * @return sorted list cars
     */
    private List<Car> sortByPrice(List<Car> cars) {
        logger.info("Cars sorting by price.");
        Comparator<Car> priceComparator = Comparator.comparing(Car::getPricePerDay);
        cars.sort(priceComparator.reversed());
        return cars;
    }

    /**
     * Method that return list of cars by different params or by default.
     * @param make car make
     * @param type car model
     * @param currentPage current page number
     * @param recordsPerPage count records per page
     * @return list of cars
     */
    private List<Car> getCars(String make, String type, int currentPage, int recordsPerPage) {
        List<Car> cars;
        if (type != null) {
            logger.info(String.format("Getting all cars with type %s.", type));
            cars = SimpleServiceFactory.getServiceFactory().getCarService().getAllWithType(type);
        } else if (make != null) {
            logger.info(String.format("Getting all cars with make %s.", make));
            cars = SimpleServiceFactory.getServiceFactory().getCarService().getAllWithMake(make);
        } else {
            logger.info("Getting all cars by default.");
            cars = SimpleServiceFactory.getServiceFactory().getCarService()
                    .getRangeOfCars((currentPage - 1) * recordsPerPage, recordsPerPage);
        }
        return cars;
    }
}
