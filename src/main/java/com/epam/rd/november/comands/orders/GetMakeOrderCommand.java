package com.epam.rd.november.comands.orders;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.resource.ConfigManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command that return make order page.
 */
public class GetMakeOrderCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(GetMakeOrderCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        String carId = req.getParameter("carId");
        if (session.getAttribute("role").equals("CLIENT") && carId != null) {
            req.getSession().setAttribute("carId", carId);
            req.getSession().setAttribute("desk", "Description");
            logger.info("Redirect to make order command");
            return ConfigManager.getProperty("path.page.order");
        } else {
            return ConfigManager.getProperty("path.page.cars");
        }
    }
}
