package com.epam.rd.november.comands;

import javax.servlet.http.HttpServletRequest;

/**
 * Basic interface for all commands.
 */
public interface ActionCommand {
    String execute(HttpServletRequest req);
}
