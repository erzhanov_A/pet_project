package com.epam.rd.november.comands.user;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.models.User;
import com.epam.rd.november.resource.ConfigManager;
import com.epam.rd.november.resource.MessageManager;
import com.epam.rd.november.services.impl.SimpleServiceFactory;
import com.epam.rd.november.util.HashPassword;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static com.epam.rd.november.comands.user.RegisterCommand.createUserFromRequest;
import static com.epam.rd.november.comands.user.RegisterCommand.validateUser;

/**
 * The command that provides updating user info in the web app.
 */
public class UpdateUserCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(UpdateUserCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        String page;
        HttpSession session = req.getSession(false);
        Integer userId = (Integer) session.getAttribute("userId");
        if (session.getAttribute("role").equals("CLIENT")) {
            User user = createUserFromRequest(req);
            user.setId(userId);

            String validResult = validateUser(user);
            if (!validResult.equals("valid")) {
                req.setAttribute("errorMessage", validResult);
                logger.info("User info validation failure.");
                page = ConfigManager.getProperty("path.page.registration");
                return page;
            }

            user.setPassword(HashPassword.hashPassword(user.getPassword()));

            boolean updateRes = SimpleServiceFactory.getServiceFactory()
                    .getUserService()
                    .update(user);
            if (!updateRes) {
                req.setAttribute("errorMessage", MessageManager.getProperty("message.user.updating.failure"));
                logger.info("Update user failed");
                page = ConfigManager.getProperty("path.page.dberror");
                return page;
            }
            page = ConfigManager.getProperty("path.page.user");
            session.setAttribute("firstname", user.getFirstName());
            session.setAttribute("lastname", user.getLastName());
            session.setAttribute("userId", user.getId());
            logger.info("User updating successful.");
            return page;

        } else {
            return ConfigManager.getProperty("path.page.index");
        }
    }
}
