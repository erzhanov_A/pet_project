package com.epam.rd.november.comands.user;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.resource.ConfigManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * The command that return register page.
 */
public class GetRegisterCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(GetRegisterCommand.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        logger.info("Redirect to registration page");
        page = ConfigManager.getProperty("path.page.registration");
        return page;
    }
}
