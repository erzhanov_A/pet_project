package com.epam.rd.november.comands.user;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.models.User;
import com.epam.rd.november.resource.ConfigManager;
import com.epam.rd.november.resource.MessageManager;
import com.epam.rd.november.services.impl.SimpleServiceFactory;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command that provides block/unblock(change status) user in the web app.
 */
public class ChangeUserStatusCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(ChangeUserStatusCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        String page;
        HttpSession session = req.getSession(false);
        if (session.getAttribute("role").equals("ADMIN")) {
            Integer userId = Integer.valueOf(req.getParameter("blockUser"));
            User.UserStatus status = User.UserStatus.valueOf(req.getParameter("userStatus"));
            boolean changeStatusRes = SimpleServiceFactory.getServiceFactory().getUserService()
                    .changeUserStatus(userId, status);
            if (!changeStatusRes) {
                req.setAttribute("errorMessage", MessageManager.getProperty("message.change.status.failure"));
                logger.info("Changing user status failed");
                page = ConfigManager.getProperty("path.page.dberror");
                return page;
            }
            page = ConfigManager.getProperty("path.page.admin");
            logger.info("Status was updated successful.");
            return page;

        } else {
            return ConfigManager.getProperty("path.page.index");
        }
    }
}
