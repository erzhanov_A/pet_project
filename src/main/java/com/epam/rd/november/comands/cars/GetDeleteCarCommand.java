package com.epam.rd.november.comands.cars;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.resource.ConfigManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command that return delete car page.
 */
public class GetDeleteCarCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(GetDeleteCarCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        String carId = req.getParameter("deleteCarId");
        if (session.getAttribute("role").equals("ADMIN") && carId != null) {
            req.getSession().setAttribute("deleteCarId", carId);
            logger.info("Redirect to delete car command");
            return ConfigManager.getProperty("path.page.deletecar");
        } else {
            return ConfigManager.getProperty("path.page.cars");
        }
    }
}
