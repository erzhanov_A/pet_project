package com.epam.rd.november.comands.cars;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.resource.ConfigManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command that return update car page.
 */
public class GetUpdateCarCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(GetUpdateCarCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        String carId = req.getParameter("updateCarId");
        if (session.getAttribute("role").equals("ADMIN") && carId != null) {
            req.getSession().setAttribute("updateCarId", carId);
            logger.info("Redirect to update car command");
            return ConfigManager.getProperty("path.page.updatecar");
        } else {
            return ConfigManager.getProperty("path.page.index");
        }
    }
}
