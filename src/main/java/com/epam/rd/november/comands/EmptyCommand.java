package com.epam.rd.november.comands;

import com.epam.rd.november.resource.ConfigManager;

import javax.servlet.http.HttpServletRequest;

/**
 * The command that return index page.
 */
public class EmptyCommand implements ActionCommand {

    @Override
    public String execute(HttpServletRequest req) {
        return ConfigManager.getProperty("path.page.index");
    }
}
