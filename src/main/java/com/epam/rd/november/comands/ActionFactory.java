package com.epam.rd.november.comands;

import com.epam.rd.november.comands.cars.*;
import com.epam.rd.november.comands.orders.*;
import com.epam.rd.november.comands.user.*;

import java.util.HashMap;
import java.util.Map;

/**
 * The factory that creates necessary command instance.
 */
public class ActionFactory {
    private Map<String, ActionCommand> actions;
    private static final ActionFactory factory = new ActionFactory();
    private static final String GET = "GET:";
    private static final String POST = "POST:";
    private final String context = "/rental-car";

    private ActionFactory() {
        actions = new HashMap<>();
        actions.put(GET + context + "/singin", new GetRegisterCommand());
        actions.put(POST + context + "/singin", new RegisterCommand());
        actions.put(GET + context + "/login", new GetLoginCommand());
        actions.put(POST + context + "/login", new LoginCommand());
        actions.put(GET + context + "/logout", new LogoutCommand());
        actions.put(GET + context + "/cars", new GetAllCarsCommand());
        actions.put(GET + context + "/neworder", new GetMakeOrderCommand());
        actions.put(POST + context + "/neworder", new MakeOrderCommand());
        actions.put(GET + context + "/orders", new GetAllOrdersCommand());
        actions.put(GET + context + "/userorders", new GetUserOrdersCommand());
        actions.put(GET + context + "/newcar", new GetCreateCarCommand());
        actions.put(POST + context + "/newcar", new CreateCarCommand());
        actions.put(GET + context + "/updatecar", new GetUpdateCarCommand());
        actions.put(POST + context + "/updatecar", new UpdateCarCommand());
        actions.put(GET + context + "/deletecar", new GetDeleteCarCommand());
        actions.put(POST + context + "/deletecar", new DeleteCarCommand());
        actions.put(GET + context + "/addcrash", new GetAddCrashCommand());
        actions.put(POST + context + "/addcrash", new AddCrashCommand());
        actions.put(GET + context + "/updatestatus", new GetUpdateOrderStatusCommand());
        actions.put(POST + context + "/updatestatus", new UpdateOrderStatusCommand());
        actions.put(GET + context + "/admin", new GetAdminPage());
        actions.put(GET + context + "/manager", new GetManagerPage());
        actions.put(GET + context + "/user", new GetUserPage());
        actions.put(GET + context + "/updateuser", new GetUpdateUserCommand());
        actions.put(POST + context + "/updateuser", new UpdateUserCommand());
        actions.put(GET + context + "/deleteuser", new GetDeleteUserCommand());
        actions.put(POST + context + "/deleteuser", new DeleteUserCommand());
        actions.put(GET + context + "/users", new GetAllUsersCommand());
        actions.put(GET + context + "/block", new GetChangeUserStatusCommand());
        actions.put(POST + context + "/block", new ChangeUserStatusCommand());
    }

    public static ActionFactory getActionFactory() {
        return factory;
    }

    /**
     * Method that defines command by key and return Command.
     * @param key request method and URI
     * @return if key is empty or not found - return EmptyCommand,
     * else return command by key value.
     */
    public ActionCommand defineCommand(String key) {
        ActionCommand emptyCommand = new EmptyCommand();
        if (key == null || key.isEmpty()) {
            return emptyCommand;
        }
        return actions.getOrDefault(key, emptyCommand);
    }
}
