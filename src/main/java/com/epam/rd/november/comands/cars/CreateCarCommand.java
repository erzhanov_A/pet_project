package com.epam.rd.november.comands.cars;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.models.Car;
import com.epam.rd.november.models.builders.CarBuilder;
import com.epam.rd.november.resource.ConfigManager;
import com.epam.rd.november.resource.MessageManager;
import com.epam.rd.november.services.impl.SimpleServiceFactory;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * The command that provides creating new car in the web app.
 */
public class CreateCarCommand implements ActionCommand {
    private final Logger logger = Logger.getLogger(CreateCarCommand.class);

    @Override
    public String execute(HttpServletRequest req) {
        String page;
        HttpSession session = req.getSession(false);
        if (session.getAttribute("role").equals("ADMIN")) {
            Car car = createCarFromRequest(req);
            boolean createRes = SimpleServiceFactory.getServiceFactory()
                    .getCarService()
                    .create(car);
            if (!createRes) {
                req.setAttribute("errorMessage", MessageManager.getProperty("message.car.creation.failure"));
                logger.info("Insert car to database failed");
                page = ConfigManager.getProperty("path.page.dberror");
                return page;
            }
            page = ConfigManager.getProperty("path.page.admin");
            logger.info("New carr added to database.");
            return page;
        } else {
            return ConfigManager.getProperty("path.page.index");
        }
    }

    static Car createCarFromRequest(HttpServletRequest req) {
        return new CarBuilder()
                .make(req.getParameter("carmake"))
                .model(req.getParameter("carmodel"))
                .type(Car.CarType.valueOf(req.getParameter("cartype")))
                .pricePerDay(Integer.parseInt(req.getParameter("price")))
                .createCar();
    }
}
