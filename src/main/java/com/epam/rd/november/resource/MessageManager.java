package com.epam.rd.november.resource;

import java.util.ResourceBundle;

/**
 * The class that manages messages in different languages.
 */
public class MessageManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("messages.messages");

    private MessageManager() {
    }

    /**
     * The method that returns messages by property.
     * @param key name message property
     * @return message
     */
    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
