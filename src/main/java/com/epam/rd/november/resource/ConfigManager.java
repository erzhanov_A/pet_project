package com.epam.rd.november.resource;

import java.util.ResourceBundle;

/**
 * The class that manages JSP pages of application.
 */
public class ConfigManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("config");

    private ConfigManager() {
    }

    /**
     * The method that returns path to JSP page.
     * @param key name of page property
     * @return path to JSP file
     */
    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
