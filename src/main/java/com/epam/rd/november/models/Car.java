package com.epam.rd.november.models;

import com.epam.rd.november.models.builders.CarBuilder;

import java.io.Serializable;

/**
 * Basic class for car entity.
 */
public class Car implements Entity, Serializable {
    private int id;
    private String make;
    private String model;
    private CarType type;
    private int pricePerDay;

    public enum CarType {
        Sedan, Wagon, Coupe, SUV, Luxury, Sportcar, Electric
    }

    public Car() {
    }

    public Car(CarBuilder builder) {
        this.id = builder.id;
        this.make = builder.make;
        this.model = builder.model;
        this.type = builder.type;
        this.pricePerDay = builder.pricePerDay;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setType(CarType type) {
        this.type = type;
    }

    public void setPricePerDay(int pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    @Override
    public int getId() {
        return id;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public CarType getType() {
        return type;
    }

    public int getPricePerDay() {
        return pricePerDay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        if (make != null ? !make.equals(car.make) : car.make != null) return false;
        if (model != null ? !model.equals(car.model) : car.model != null) return false;
        return type == car.type;
    }

    @Override
    public int hashCode() {
        int result = make != null ? make.hashCode() : 0;
        result = 31 * result + (model != null ? model.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Car{" +
                "make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", type=" + type +
                ", pricePerDay=" + pricePerDay +
                '}';
    }
}