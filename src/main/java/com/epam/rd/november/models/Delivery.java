package com.epam.rd.november.models;

import java.io.Serializable;

/**
 * Basic class for delivery entity.
 */
public class Delivery implements Entity, Serializable {
    private int id;
    private String city;
    private String address;

    public Delivery() {
    }

    public Delivery(String city, String address) {
        this.city = city;
        this.address = address;
    }

    public Delivery(int id, String city, String address) {
        this.id = id;
        this.city = city;
        this.address = address;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
