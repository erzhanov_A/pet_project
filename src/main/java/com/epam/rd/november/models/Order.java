package com.epam.rd.november.models;

import com.epam.rd.november.models.builders.OrderBuilder;

import java.io.Serializable;
import java.util.Date;

/**
 * Basic class for order entity.
 */
public class Order implements Entity, Serializable {
    private int id;
    private Delivery address;
    private Crash crash;
    private Date firstDay;
    private Date lastDay;
    private Car car;
    private User user;
    private Date created;
    private Status status;
    private String description;
    private int bill;
    private boolean withDriver;

    public enum Status {
        CREATED, ACCEPTED, REJECTED, PAID, INPROGRESS, CLOSED
    }

    public Order() {
    }

    public Order(OrderBuilder builder) {
        this.id = builder.id;
        this.address = builder.address;
        this.crash = builder.crash;
        this.firstDay = builder.firstDay;
        this.lastDay = builder.lastDay;
        this.car = builder.car;
        this.description = builder.description;
        this.user = builder.user;
        this.created = builder.created;
        this.status = builder.status;
        this.bill = builder.bill;
        this.withDriver = builder.withDriver;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public Delivery getAddress() {
        return address;
    }

    public void setAddress(Delivery address) {
        this.address = address;
    }

    public Crash getCrash() {
        return crash;
    }

    public void setCrash(Crash crash) {
        this.crash = crash;
    }

    public Date getFirstDay() {
        return firstDay;
    }

    public void setFirstDay(Date firstDay) {
        this.firstDay = firstDay;
    }

    public Date getLastDay() {
        return lastDay;
    }

    public void setLastDay(Date lastDay) {
        this.lastDay = lastDay;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getBill() {
        return bill;
    }

    public void setBill(int bill) {
        this.bill = bill;
    }

    public boolean isWithDriver() {
        return withDriver;
    }

    public void setWithDriver(boolean withDriver) {
        this.withDriver = withDriver;
    }
}
