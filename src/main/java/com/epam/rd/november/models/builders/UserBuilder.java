package com.epam.rd.november.models.builders;

import com.epam.rd.november.models.User;

public class UserBuilder {
    public int id;
    public String firstName;
    public String lastName;
    public String email;
    public String document;
    public String login;
    public String password;
    public User.Role role;
    public User.UserStatus userStatus;

    public UserBuilder() {
    }

    public UserBuilder id(int id) {
        this.id = id;
        return this;
    }

    public UserBuilder firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public UserBuilder lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public UserBuilder email(String email) {
        this.email = email;
        return this;
    }

    public UserBuilder document(String document) {
        this.document = document;
        return this;
    }

    public UserBuilder login(String login) {
        this.login = login;
        return this;
    }

    public UserBuilder password(String password) {
        this.password = password;
        return this;
    }

    public UserBuilder role(User.Role role) {
        this.role = role;
        return this;
    }

    public UserBuilder userStatus(User.UserStatus userStatus) {
        this.userStatus = userStatus;
        return this;
    }

    public User createUser() {
        return new User(this);
    }
}
