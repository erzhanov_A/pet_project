package com.epam.rd.november.models.builders;

import com.epam.rd.november.models.*;

import java.util.Date;

public class OrderBuilder {
    public int id;
    public Delivery address;
    public Crash crash;
    public Date firstDay;
    public Date lastDay;
    public Car car;
    public User user;
    public String description;
    public Date created;
    public Order.Status status;
    public int bill;
    public boolean withDriver;

    public OrderBuilder() {
    }

    public OrderBuilder id(int id) {
        this.id = id;
        return this;
    }

    public OrderBuilder address(Delivery address) {
        this.address = address;
        return this;
    }

    public OrderBuilder firstDay(Date firstDay) {
        this.firstDay = firstDay;
        return this;
    }

    public OrderBuilder lastDay(Date lastDay) {
        this.lastDay = lastDay;
        return this;
    }

    public OrderBuilder created(Date created) {
        this.created = created;
        return this;
    }

    public OrderBuilder car(Car car) {
        this.car = car;
        return this;
    }

    public OrderBuilder user(User user) {
        this.user = user;
        return this;
    }

    public OrderBuilder crash(Crash crash) {
        this.crash = crash;
        return this;
    }

    public OrderBuilder status(Order.Status status) {
        this.status = status;
        return this;
    }

    public OrderBuilder description(String description) {
        this.description = description;
        return this;
    }

    public OrderBuilder bill(int bill) {
        this.bill = bill;
        return this;
    }

    public OrderBuilder withDriver(boolean withDriver) {
        this.withDriver = withDriver;
        return this;
    }

    public Order createOrder() {
        return new Order(this);
    }
}
