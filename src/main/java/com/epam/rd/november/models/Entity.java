package com.epam.rd.november.models;

/**
 * Public interface for entity.
 */
public interface Entity {
    void setId(int id);

    int getId();
}
