package com.epam.rd.november.models;

import java.io.Serializable;

/**
 * Basic class for crash entity.
 */
public class Crash implements Entity, Serializable {
    private int id;
    private int damageSum;
    private String crashDescription;
    private boolean paid;

    public Crash() {
    }

    public Crash(int damageSum, boolean isPaid) {
        this.damageSum = damageSum;
        this.paid = isPaid;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public int getDamageSum() {
        return damageSum;
    }

    public void setDamageSum(int damageSum) {
        this.damageSum = damageSum;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setIsPaid(boolean paid) {
        this.paid = paid;
    }

    public String getCrashDescription() {
        return crashDescription;
    }

    public void setCrashDescription(String crashDescription) {
        this.crashDescription = crashDescription;
    }
}
