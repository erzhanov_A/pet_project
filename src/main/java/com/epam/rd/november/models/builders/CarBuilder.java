package com.epam.rd.november.models.builders;

import com.epam.rd.november.models.Car;

public class CarBuilder {
    public int id;
    public String make;
    public String model;
    public Car.CarType type;
    public int pricePerDay;

    public CarBuilder() {
    }

    public CarBuilder id(int id) {
        this.id = id;
        return this;
    }

    public CarBuilder make(String make) {
        this.make = make;
        return this;
    }

    public CarBuilder model(String model) {
        this.model = model;
        return this;
    }

    public CarBuilder type(Car.CarType type) {
        this.type = type;
        return this;
    }

    public CarBuilder pricePerDay(int pricePerDay) {
        this.pricePerDay = pricePerDay;
        return this;
    }

    public Car createCar() {
        return new Car(this);
    }
}
