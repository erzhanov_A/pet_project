package com.epam.rd.november.services.impl;

import com.epam.rd.november.dao.impl.SimpleDaoFactory;
import com.epam.rd.november.models.Delivery;
import com.epam.rd.november.services.Service;

import java.util.List;

/**
 * The class provides operations with delivery in the web application.
 */
public class DeliveryService implements Service<Delivery> {
    @Override
    public boolean create(Delivery entity) {
        return SimpleDaoFactory.getDaoFactory().getDeliveryDao().create(entity);
    }

    @Override
    public Delivery read(int id) {
        return SimpleDaoFactory.getDaoFactory().getDeliveryDao().read(id);
    }

    @Override
    public boolean update(Delivery entity) {
        return SimpleDaoFactory.getDaoFactory().getDeliveryDao().update(entity);
    }

    @Override
    public boolean delete(int id) {
        return SimpleDaoFactory.getDaoFactory().getDeliveryDao().delete(id);
    }

    @Override
    public List<Delivery> getAll() {
        return SimpleDaoFactory.getDaoFactory().getDeliveryDao().getAll();
    }

    @Override
    public int getCount() {
        return SimpleDaoFactory.getDaoFactory().getDeliveryDao().getCount();
    }
}
