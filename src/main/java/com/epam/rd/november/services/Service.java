package com.epam.rd.november.services;

import com.epam.rd.november.models.Entity;

import java.util.List;

/**
 * Basic interface for services.
 * @param <T> type of service
 */
public interface Service<T extends Entity> {
    boolean create(T entity);

    T read(int id);

    boolean update(T entity);

    boolean delete(int id);

    List<T> getAll();

    int getCount();
}
