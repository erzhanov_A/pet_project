package com.epam.rd.november.services.impl;

import com.epam.rd.november.services.ServiceFactory;

public class SimpleServiceFactory implements ServiceFactory {
    private static final SimpleServiceFactory factory = new SimpleServiceFactory();

    public static SimpleServiceFactory getServiceFactory() {
        return factory;
    }

    @Override
    public UserService getUserService() {
        return new UserService();
    }

    @Override
    public CarService getCarService() {
        return new CarService();
    }

    @Override
    public OrderService getOrderService() {
        return new OrderService();
    }

    @Override
    public CrashService getCrashService() {
        return new CrashService();
    }

    @Override
    public DeliveryService getDeliveryService() {
        return new DeliveryService();
    }
}
