package com.epam.rd.november.services.impl;

import com.epam.rd.november.dao.impl.SimpleDaoFactory;
import com.epam.rd.november.models.Crash;
import com.epam.rd.november.models.Order;
import com.epam.rd.november.models.User;
import com.epam.rd.november.services.Service;

import java.util.List;

/**
 * The class provides operations with orders in the web application.
 */
public class OrderService implements Service<Order> {
    @Override
    public boolean create(Order entity) {
        return SimpleDaoFactory.getDaoFactory().getOrderDao().create(entity);
    }

    @Override
    public Order read(int id) {
        return SimpleDaoFactory.getDaoFactory().getOrderDao().read(id);
    }

    @Override
    public boolean update(Order entity) {
        return SimpleDaoFactory.getDaoFactory().getOrderDao().update(entity);
    }

    @Override
    public boolean delete(int id) {
        return SimpleDaoFactory.getDaoFactory().getOrderDao().delete(id);
    }

    @Override
    public List<Order> getAll() {
        return SimpleDaoFactory.getDaoFactory().getOrderDao().getAll();
    }

    @Override
    public int getCount() {
        return SimpleDaoFactory.getDaoFactory().getOrderDao().getCount();
    }

    public List<Order> getRangeOfOrders(int startIndex, int count) {
        return SimpleDaoFactory.getDaoFactory().getOrderDao().getRangeOfOrders(startIndex, count);
    }

    public boolean addCrash(Crash crash, Order order) {
        return SimpleDaoFactory.getDaoFactory().getOrderDao().addCrash(crash, order);
    }

    public boolean updateStatus(Order order) {
        return SimpleDaoFactory.getDaoFactory().getOrderDao().updateStatus(order);
    }

    public List<Order> getUserOrders(int userId) {
        return SimpleDaoFactory.getDaoFactory().getOrderDao().getAllUserOrders(userId);
    }

    public void deleteAllOrders() {
        SimpleDaoFactory.getDaoFactory().getOrderDao().deleteAllOrders();
    }
}
