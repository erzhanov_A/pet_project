package com.epam.rd.november.services.impl;

import com.epam.rd.november.dao.impl.SimpleDaoFactory;
import com.epam.rd.november.models.Crash;
import com.epam.rd.november.services.Service;

import java.util.List;

/**
 * The class provides operations with crash in the web application.
 */
public class CrashService implements Service<Crash> {
    @Override
    public boolean create(Crash entity) {
        return SimpleDaoFactory.getDaoFactory().getCrashDao().create(entity);
    }

    @Override
    public Crash read(int id) {
        return SimpleDaoFactory.getDaoFactory().getCrashDao().read(id);
    }

    @Override
    public boolean update(Crash entity) {
        return SimpleDaoFactory.getDaoFactory().getCrashDao().update(entity);
    }

    @Override
    public boolean delete(int id) {
        return SimpleDaoFactory.getDaoFactory().getCrashDao().delete(id);
    }

    @Override
    public List<Crash> getAll() {
        return SimpleDaoFactory.getDaoFactory().getCrashDao().getAll();
    }

    @Override
    public int getCount() {
        return SimpleDaoFactory.getDaoFactory().getCrashDao().getCount();
    }
}
