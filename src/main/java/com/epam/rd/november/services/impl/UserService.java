package com.epam.rd.november.services.impl;

import com.epam.rd.november.dao.impl.SimpleDaoFactory;
import com.epam.rd.november.models.User;
import com.epam.rd.november.services.Service;

import java.util.List;

/**
 * The class provides operations with users in the web application.
 */
public class UserService implements Service<User> {
    @Override
    public boolean create(User entity) {
        return SimpleDaoFactory.getDaoFactory().getUserDao().create(entity);
    }

    @Override
    public User read(int id) {
        return SimpleDaoFactory.getDaoFactory().getUserDao().read(id);
    }

    @Override
    public boolean update(User entity) {
        return SimpleDaoFactory.getDaoFactory().getUserDao().update(entity);
    }

    @Override
    public boolean delete(int id) {
        return SimpleDaoFactory.getDaoFactory().getUserDao().delete(id);
    }

    @Override
    public List<User> getAll() {
        return SimpleDaoFactory.getDaoFactory().getUserDao().getAll();
    }

    @Override
    public int getCount() {
        return SimpleDaoFactory.getDaoFactory().getUserDao().getCount();
    }

    public void deleteAllUsers() {
        SimpleDaoFactory.getDaoFactory().getUserDao().deleteAllUsers();
    }

    public User getByLogin(String login) {
        return SimpleDaoFactory.getDaoFactory().getUserDao().getByLogin(login);
    }

    public boolean changeUserStatus(int userId, User.UserStatus status) {
        return SimpleDaoFactory.getDaoFactory().getUserDao().changeUserStatus(userId, status);
    }

    public boolean addDocument(String document, String login) {
        return SimpleDaoFactory.getDaoFactory().getUserDao().addDocument(document, login);
    }

    public List<User> getRangeOfUsers(int startIndex, int count) {
        return SimpleDaoFactory.getDaoFactory().getUserDao().getRangeOfUsers(startIndex, count);
    }
}
