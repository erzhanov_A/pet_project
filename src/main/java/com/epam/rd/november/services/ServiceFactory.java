package com.epam.rd.november.services;

import com.epam.rd.november.services.impl.*;

public interface ServiceFactory {
    UserService getUserService();

    CarService getCarService();

    OrderService getOrderService();

    CrashService getCrashService();

    DeliveryService getDeliveryService();
}
