package com.epam.rd.november.controller;

import com.epam.rd.november.comands.ActionCommand;
import com.epam.rd.november.comands.ActionFactory;
import com.epam.rd.november.resource.ConfigManager;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Controller that process all requests.
 */
public class ManagerServlet extends HttpServlet {
    private final Logger logger = Logger.getLogger(ManagerServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        logger.info("Processing request......");
        req.setCharacterEncoding("UTF-8");
        String key = getMethod(req) + ":" + getUri(req);
        ActionCommand command = ActionFactory.getActionFactory().defineCommand(key);
        String page = command.execute(req);
        logger.info(String.format("Servlet get page: %s", page));
        if (page != null) {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
            dispatcher.forward(req, resp);
        } else {
            page = ConfigManager.getProperty("path.page.index");
            resp.sendRedirect(req.getContextPath() + page);
        }
    }

    private String getMethod(HttpServletRequest request) {
        return request.getMethod().toUpperCase();
    }

    private String getUri(HttpServletRequest request) {
        String uri = request.getRequestURI();
        return uri.toLowerCase();
    }
}
