package com.epam.rd.november.filters;

import java.util.Locale;

/**
 * This class holds all supported locales in the web app.
 */
public class LocaleHolder {
    private Locale currentLocale;

    public static final Locale[] locales = {
            new Locale("en", "EN"),
            new Locale("ru", "RU"),
    };


    public LocaleHolder() {
    }

    public Locale getCurrentLocale() {
        return currentLocale;
    }

    public void setCurrentLocale(Locale currentLocale) {
        this.currentLocale = currentLocale;
    }

    public Locale getDefaultLocale() {
        return new Locale("ru", "RU");
    }
}
