package com.epam.rd.november.comands.user;

import com.epam.rd.november.resource.ConfigManager;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static util.TestUtil.createUserForIT;

public class ChangeUserStatusCommandIT {

    private HttpServletRequest req = mock(HttpServletRequest.class);
    private ChangeUserStatusCommand command = new ChangeUserStatusCommand();
    private HttpSession session = mock(HttpSession.class);

    @Test
    public void changeStatusWithAdminRole() {
        doReturn(session).when(req).getSession(false);
        when(session.getAttribute("role")).thenReturn("ADMIN");
        int id = createUserForIT("changestatus@gmail.com", "changest").getId();
        when(req.getParameter("blockUser")).thenReturn(String.valueOf(id));
        when(req.getParameter("userStatus")).thenReturn("Blocked");
        String page = command.execute(req);
        assertEquals(ConfigManager.getProperty("path.page.admin"), page);
    }

    @Test
    public void changeStatusWithOtherRole() {
        doReturn(session).when(req).getSession(false);
        when(session.getAttribute("role")).thenReturn("MANAGER");
        int id = createUserForIT("changestatus1@gmail.com", "changest1").getId();
        when(req.getParameter("blockUser")).thenReturn(String.valueOf(id));
        when(req.getParameter("userStatus")).thenReturn("Blocked");
        String page = command.execute(req);
        assertEquals(ConfigManager.getProperty("path.page.index"), page);
    }

}