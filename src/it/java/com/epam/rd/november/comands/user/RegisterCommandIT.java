package com.epam.rd.november.comands.user;

import com.epam.rd.november.comands.user.RegisterCommand;
import com.epam.rd.november.models.User;
import com.epam.rd.november.models.builders.UserBuilder;
import com.epam.rd.november.resource.ConfigManager;
import com.epam.rd.november.resource.MessageManager;
import com.epam.rd.november.services.impl.SimpleServiceFactory;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;


public class RegisterCommandIT {
	private HttpServletRequest req = mock(HttpServletRequest.class);
	private HttpSession session = mock(HttpSession.class);
	private RegisterCommand command = new RegisterCommand();

	@Test
	public void registerValidAndNewUser() {
		when(req.getParameter("login")).thenReturn("denis");
		when(req.getParameter("email")).thenReturn("denis@gmail.com");
		when(req.getParameter("password")).thenReturn("Denis54");
		when(req.getParameter("firstname")).thenReturn("Denis");
		when(req.getParameter("lastname")).thenReturn("Terry");
		when(req.getSession()).thenReturn(session);
		String page = command.execute(req);
		assertEquals(ConfigManager.getProperty("path.page.success"), page);
		verify(session).setAttribute("firstname", "Denis");
		verify(session).setAttribute("lastname", "Terry");
	}

	@Test
	public void registerAlreadyExistUser() {
		User user = new UserBuilder()
				.firstName("Alex")
				.lastName("Tyler")
				.login("tyler")
				.email("tyler@gmail.com").password("Tyler34")
				.role(User.Role.CLIENT).createUser();
		SimpleServiceFactory.getServiceFactory().getUserService().create(user);
		when(req.getParameter("login")).thenReturn(user.getLogin());
		when(req.getParameter("email")).thenReturn(user.getEmail());
		when(req.getParameter("password")).thenReturn(user.getPassword());
		when(req.getParameter("firstname")).thenReturn(user.getFirstName());
		when(req.getParameter("lastname")).thenReturn(user.getLastName());
		String page = command.execute(req);
		verify(req).setAttribute("errorMessage", MessageManager.getProperty("message.user.creation.failure"));
		assertEquals(ConfigManager.getProperty("path.page.dberror"), page);
	}
}