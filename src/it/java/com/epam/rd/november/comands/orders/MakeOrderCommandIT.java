package com.epam.rd.november.comands.orders;

import com.epam.rd.november.resource.ConfigManager;
import com.epam.rd.november.resource.MessageManager;
import org.junit.Test;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static util.TestUtil.createUserForIT;

public class MakeOrderCommandIT {
    private HttpServletRequest req = mock(HttpServletRequest.class);
    private HttpSession session = mock(HttpSession.class);
    private MakeOrderCommand command = new MakeOrderCommand();


    @Test
    public void invalidTime() {
        when(req.getSession()).thenReturn(session);
        int id = createUserForIT("order1@gmail.com", "ordertest1").getId();
        when(session.getAttribute("userId")).thenReturn(id);
        when(req.getParameter("carId")).thenReturn("1");
        when(req.getParameter("city")).thenReturn("Odesa");
        when(req.getParameter("address")).thenReturn("Shevchenka 25");
        when(req.getParameter("document")).thenReturn("AO 123233");
        when(req.getParameter("firstdate")).thenReturn("2018-02-13");
        when(req.getParameter("lastdate")).thenReturn("2018-02-11");
        String page = command.execute(req);
        verify(req).setAttribute("errorMessage", MessageManager.getProperty("message.rent.time.invalid"));
        assertEquals(ConfigManager.getProperty("path.page.order"), page);
    }

    @Test
    public void invalidEqualTime() {
        when(req.getSession()).thenReturn(session);
        int id = createUserForIT("order2@gmail.com", "ordertest2").getId();
        when(session.getAttribute("userId")).thenReturn(id);
        when(req.getParameter("carId")).thenReturn("3");
        when(req.getParameter("city")).thenReturn("Dnipro");
        when(req.getParameter("address")).thenReturn("Budivnikiv 3");
        when(req.getParameter("document")).thenReturn("AP 7654536");
        when(req.getParameter("firstdate")).thenReturn("2018-02-13");
        when(req.getParameter("lastdate")).thenReturn("2018-02-13");
        String page = command.execute(req);
        verify(req).setAttribute("errorMessage", MessageManager.getProperty("message.rent.time.invalid"));
        assertEquals(ConfigManager.getProperty("path.page.order"), page);
    }

    @Test
    public void validFormParams() {
        when(req.getSession()).thenReturn(session);
        int id = createUserForIT("order3@gmail.com", "ordertest3").getId();
        when(session.getAttribute("userId")).thenReturn(id);
        when(req.getParameter("carId")).thenReturn("1");
        when(req.getParameter("city")).thenReturn("Lviv");
        when(req.getParameter("address")).thenReturn("Shevchenka 23/4");
        when(req.getParameter("document")).thenReturn("AO 4343434");
        when(req.getParameter("firstdate")).thenReturn("2018-02-13");
        when(req.getParameter("lastdate")).thenReturn("2018-02-16");
        String page = command.execute(req);
        assertEquals(ConfigManager.getProperty("path.page.user"), page);
    }
}