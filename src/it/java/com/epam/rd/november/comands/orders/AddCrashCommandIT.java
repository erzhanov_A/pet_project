package com.epam.rd.november.comands.orders;

import com.epam.rd.november.models.Order;
import com.epam.rd.november.resource.ConfigManager;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static util.TestUtil.createOrder;

public class AddCrashCommandIT {
    private HttpServletRequest req = mock(HttpServletRequest.class);
    private AddCrashCommand command = new AddCrashCommand();
    private HttpSession session = mock(HttpSession.class);

    @Test
    public void createNewCrashWithManagerRole() {
        doReturn(session).when(req).getSession(false);
        when(session.getAttribute("role")).thenReturn("MANAGER");
        Order order = createOrder("crashorder1@gmail.com", "ordercrash1");
        when(req.getParameter("orderId")).thenReturn(String.valueOf(order.getId()));
        when(req.getParameter("damageSum")).thenReturn("5000");
        when(req.getParameter("crashDescription")).thenReturn("Two doors are brocken");
        when(req.getParameter("isPaid")).thenReturn("false");
        String page = command.execute(req);
        assertEquals(ConfigManager.getProperty("path.page.manager"), page);
    }

    @Test
    public void createNewCrashWithOtherRole() {
        doReturn(session).when(req).getSession(false);
        when(session.getAttribute("role")).thenReturn("CLIENT");
        when(req.getParameter("orderId")).thenReturn("1");
        String page = command.execute(req);
        assertEquals(ConfigManager.getProperty("path.page.index"), page);
    }
}