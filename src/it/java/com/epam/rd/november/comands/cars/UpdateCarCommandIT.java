package com.epam.rd.november.comands.cars;

import com.epam.rd.november.models.Car;
import com.epam.rd.november.resource.ConfigManager;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static util.TestUtil.createCar;

public class UpdateCarCommandIT {
    private HttpServletRequest req = mock(HttpServletRequest.class);
    private CreateCarCommand command = new CreateCarCommand();
    private HttpSession session = mock(HttpSession.class);

    @Test
    public void updateCarWithAdminRole() {
        doReturn(session).when(req).getSession(false);
        when(session.getAttribute("role")).thenReturn("ADMIN");
        Car car = createCar();
        when(req.getParameter("updateCarId")).thenReturn(String.valueOf(car.getId()));
        when(req.getParameter("carmake")).thenReturn("Jeep");
        when(req.getParameter("carmodel")).thenReturn("Grand");
        when(req.getParameter("cartype")).thenReturn("SUV");
        when(req.getParameter("price")).thenReturn("8000");
        String page = command.execute(req);
        assertEquals(ConfigManager.getProperty("path.page.admin"), page);
    }

    @Test
    public void updateNewCarWithOtherRole() {
        doReturn(session).when(req).getSession(false);
        when(session.getAttribute("role")).thenReturn("CLIENT");
        when(req.getParameter("updateCarId")).thenReturn("1");
        String page = command.execute(req);
        assertEquals(ConfigManager.getProperty("path.page.index"), page);
    }
}