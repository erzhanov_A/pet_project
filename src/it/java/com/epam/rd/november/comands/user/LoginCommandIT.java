package com.epam.rd.november.comands.user;

import com.epam.rd.november.models.User;
import com.epam.rd.november.resource.ConfigManager;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static util.TestUtil.createUserForIT;

public class LoginCommandIT {
    private HttpServletRequest req = mock(HttpServletRequest.class);
    private HttpSession session = mock(HttpSession.class);
    private LoginCommand command = new LoginCommand();

    @Test
    public void emptyFormParams() {
        when(req.getParameter("login")).thenReturn("");
        when(req.getParameter("password")).thenReturn("");
        String page = command.execute(req);
        assertEquals(ConfigManager.getProperty("path.page.login"), page);
    }

    @Test
    public void validLoginAndPass() {
        User user = createUserForIT("login1@gmail.com", "login1");
        when(req.getParameter("login")).thenReturn("login1");
        when(req.getParameter("password")).thenReturn("Test1");
        when(req.getSession()).thenReturn(session);
        String page = command.execute(req);
        assertEquals(ConfigManager.getProperty("path.page.user"), page);
        verify(session).setAttribute("firstname", user.getFirstName());
        verify(session).setAttribute("lastname", user.getLastName());
        session.setAttribute("userId", user.getId());
        session.setAttribute("role", user.getRole().toString());
    }

    @Test
    public void validLoginAndWrongPass() {
        createUserForIT("login2@gmail.com", "login2");
        when(req.getParameter("login")).thenReturn("login2");
        when(req.getParameter("password")).thenReturn("Test");
        String page = command.execute(req);
        assertEquals(ConfigManager.getProperty("path.page.login"), page);
    }

    @Test
    public void wrongLogin() {
        createUserForIT("login3@gmail.com", "login3");
        when(req.getParameter("login")).thenReturn("wrlogin");
        when(req.getParameter("password")).thenReturn("Test1");
        String page = command.execute(req);
        assertEquals(ConfigManager.getProperty("path.page.login"), page);
    }

}