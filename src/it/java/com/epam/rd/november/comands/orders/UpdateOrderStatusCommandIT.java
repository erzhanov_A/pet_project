package com.epam.rd.november.comands.orders;

import com.epam.rd.november.models.Order;
import com.epam.rd.november.resource.ConfigManager;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static util.TestUtil.createOrder;

public class UpdateOrderStatusCommandIT {
    private HttpServletRequest req = mock(HttpServletRequest.class);
    private UpdateOrderStatusCommand command = new UpdateOrderStatusCommand();
    private HttpSession session = mock(HttpSession.class);

    @Test
    public void updateStatusWithManagerRole() {
        doReturn(session).when(req).getSession(false);
        when(session.getAttribute("role")).thenReturn("MANAGER");
        Order order = createOrder("orderstatus@gmail.com", "orderstatus");
        when(req.getParameter("updateStatusId")).thenReturn(String.valueOf(order.getId()));
        when(req.getParameter("status")).thenReturn("REJECTED");
        when(req.getParameter("description")).thenReturn("Sorry but car is not available.");
        String page = command.execute(req);
        assertEquals(ConfigManager.getProperty("path.page.manager"), page);
    }

    @Test
    public void updateStatusWithOtherRole() {
        doReturn(session).when(req).getSession(false);
        when(session.getAttribute("role")).thenReturn("CLIENT");
        String page = command.execute(req);
        assertEquals(ConfigManager.getProperty("path.page.index"), page);
    }
}