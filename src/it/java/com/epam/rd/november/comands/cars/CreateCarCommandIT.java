package com.epam.rd.november.comands.cars;

import com.epam.rd.november.resource.ConfigManager;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CreateCarCommandIT {

    private HttpServletRequest req = mock(HttpServletRequest.class);
    private CreateCarCommand command = new CreateCarCommand();
    private HttpSession session = mock(HttpSession.class);

    @Test
    public void createNewCarWithAdminRole() {
        doReturn(session).when(req).getSession(false);
        when(session.getAttribute("role")).thenReturn("ADMIN");
        when(req.getParameter("carmake")).thenReturn("Ford");
        when(req.getParameter("carmodel")).thenReturn("Mustang");
        when(req.getParameter("cartype")).thenReturn("Sedan");
        when(req.getParameter("price")).thenReturn("2000");
        String page = command.execute(req);
        assertEquals(ConfigManager.getProperty("path.page.admin"), page);
    }

    @Test
    public void createNewCarWithOtherRole() {
        doReturn(session).when(req).getSession(false);
        when(session.getAttribute("role")).thenReturn("CLIENT");
        when(req.getParameter("carmake")).thenReturn("Ford");
        when(req.getParameter("carmodel")).thenReturn("GT");
        when(req.getParameter("cartype")).thenReturn("Sedan");
        when(req.getParameter("price")).thenReturn("3000");
        String page = command.execute(req);
        assertEquals(ConfigManager.getProperty("path.page.index"), page);
    }
}