package com.epam.rd.november.dao.impl;

import com.epam.rd.november.models.Crash;
import org.junit.Test;


import static org.junit.Assert.*;

public class CrashDaoIT {
	private final CrashDao myDao = SimpleDaoFactory.getDaoFactory().getCrashDao();


	@Test
	public void createCrash() {
		Crash crash = new Crash();
		crash.setDamageSum(500);
		crash.setCrashDescription("Broken lights");
		crash.setIsPaid(true);
		assertTrue(myDao.create(crash));
		assertEquals(500, myDao.read(crash.getId()).getDamageSum());
		assertEquals(true, myDao.read(crash.getId()).isPaid());
	}

	@Test
	public void readCrash() {
		Crash crash = new Crash();
		crash.setDamageSum(2000);
        crash.setCrashDescription("Broken window");
		crash.setIsPaid(false);
		assertTrue(myDao.create(crash));
		Crash readCrash = myDao.read(crash.getId());
		assertEquals(crash.getDamageSum(), readCrash.getDamageSum());
		assertEquals(crash.isPaid(), readCrash.isPaid());
	}

	@Test
	public void updateCrash() {
		Crash crash1 = new Crash();
		crash1.setDamageSum(1000);
		crash1.setIsPaid(false);
		assertTrue(myDao.create(crash1));

		Crash crash2 = new Crash();
		crash2.setDamageSum(1000);
        crash2.setCrashDescription("Broken sit belt");
		crash2.setIsPaid(true);
		crash2.setId(crash1.getId());
		assertTrue(myDao.update(crash2));
		assertEquals(1000, myDao.read(crash1.getId()).getDamageSum());
		assertEquals(true, myDao.read(crash1.getId()).isPaid());
	}

	@Test
	public void removeUser() {
		Crash crash = new Crash();
		crash.setDamageSum(2000);
		crash.setIsPaid(false);
		assertTrue(myDao.create(crash));
		assertTrue(myDao.delete(crash.getId()));
		assertNull(myDao.read(crash.getId()));
	}
}