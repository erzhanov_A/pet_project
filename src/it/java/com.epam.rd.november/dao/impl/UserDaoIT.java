package com.epam.rd.november.dao.impl;

import com.epam.rd.november.models.User;
import com.epam.rd.november.models.builders.UserBuilder;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


public class UserDaoIT {
	private final UserDao myDao = SimpleDaoFactory.getDaoFactory().getUserDao();

	@Before
	public void beforeTests() {
		OrderDao orderDao = new OrderDao();
		orderDao.deleteAllOrders();
		UserDao dao = new UserDao();
		dao.deleteAllUsers();
	}

	@Test
	public void createUser() {
		User user = new UserBuilder()
				.firstName("Ivan")
				.lastName("Petrov")
				.login("petrov")
				.email("ivan@gmail.com")
				.password("0000")
				.createUser();
		assertTrue(myDao.create(user));
		assertEquals("Ivan", myDao.read(user.getId()).getFirstName());
		assertEquals("Petrov", myDao.read(user.getId()).getLastName());
		assertEquals("petrov", myDao.read(user.getId()).getLogin());
		assertEquals("ivan@gmail.com", myDao.read(user.getId()).getEmail());
		assertEquals("0000", myDao.read(user.getId()).getPassword());
	}


	@Test
	public void readUser() {
		User user = new UserBuilder()
				.firstName("Kate")
				.lastName("Smith")
				.login("katy")
				.email("kate@gmail.com")
				.password("0000")
				.createUser();
		assertTrue(myDao.create(user));
		User readUser = myDao.read(user.getId());
		assertEquals(user.getFirstName(), readUser.getFirstName());
		assertEquals(user.getLastName(), readUser.getLastName());
		assertEquals(user.getEmail(), readUser.getEmail());
		assertEquals(user.getPassword(), readUser.getPassword());
	}

	@Test
	public void updateUser() {
		User user1 = new UserBuilder()
				.firstName("Alex")
				.lastName("Bredly")
				.login("alex")
				.email("alex@gmail.com")
				.password("0000")
				.createUser();
		myDao.create(user1);

		User user2 = new UserBuilder()
				.id(user1.getId())
				.firstName("Mike")
				.lastName("Willson")
				.login("mike")
				.email("mike@gmail.com")
				.password("1111")
				.createUser();
		assertTrue(myDao.update(user2));
		assertEquals("Mike", myDao.read(user1.getId()).getFirstName());
		assertEquals("Willson", myDao.read(user1.getId()).getLastName());
		assertEquals("mike@gmail.com", myDao.read(user1.getId()).getEmail());
		assertEquals("1111", myDao.read(user1.getId()).getPassword());
	}

	@Test
	public void removeUser() {
		User user = new UserBuilder()
				.firstName("Nick")
				.lastName("Moris")
				.login("nick")
				.email("nick@gmail.com")
				.password("2342")
				.createUser();
		assertTrue(myDao.create(user));
		assertTrue(myDao.delete(user.getId()));
		assertNull(myDao.read(user.getId()));
	}

	@Test
	public void getAll() {
		List<User> list = getListUsers();
		int beforeCount = myDao.getCount();
		for (User user : list) {
			myDao.create(user);
		}
		assertEquals(list.size(), (myDao.getAll().size() - beforeCount));
	}

	private static List<User> getListUsers() {
		User user1 = new UserBuilder()
				.firstName("David")
				.lastName("Moris")
				.login("david")
				.email("david@gmail.com")
				.password("3434")
				.createUser();
		User user2 = new UserBuilder()
				.firstName("Jane")
				.lastName("Moris")
				.login("jane")
				.email("jane@gmail.com")
				.password("2342")
				.createUser();
		User user3 = new UserBuilder()
				.firstName("Stiv")
				.lastName("Moris")
				.login("stiv")
				.email("stiv@gmail.com")
				.password("34353")
				.createUser();
		List<User> list = new ArrayList<>();
		list.add(user1);
		list.add(user2);
		list.add(user3);
		return list;
	}
}