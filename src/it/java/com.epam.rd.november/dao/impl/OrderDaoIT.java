package com.epam.rd.november.dao.impl;

import com.epam.rd.november.models.Order;
import com.epam.rd.november.models.builders.OrderBuilder;
import org.junit.Test;

import static org.junit.Assert.*;
import static util.TestUtil.*;

public class OrderDaoIT {
	private final OrderDao myDao = SimpleDaoFactory.getDaoFactory().getOrderDao();

	@Test
	public void createOrder() {
		Order order = new OrderBuilder()
				.user(createUserForIT("order@gmail.com", "ordertest"))
				.car(createCar())
				.address(createDelivery())
				.crash(createCrash())
				.description("desk")
				.firstDay(createDate(1, 05,2018))
				.lastDay(createDate(7,05,2018))
				.withDriver(true)
				.bill(2323)
				.status(Order.Status.CREATED)
				.createOrder();

		assertTrue(myDao.create(order));
		assertEquals(true, myDao.read(order.getId()).isWithDriver());
		assertTrue(myDao.delete(order.getId()));
	}
}