package com.epam.rd.november.dao.impl;

import com.epam.rd.november.models.Car;
import com.epam.rd.november.models.builders.CarBuilder;
import com.epam.rd.november.services.impl.SimpleServiceFactory;
import org.junit.Test;

import static org.junit.Assert.*;

public class CarDaoIT {
	private final CarDao myDao = SimpleDaoFactory.getDaoFactory().getCarDao();
	@Test
	public void create() {
		Car car1 = new CarBuilder()
				.make("BMW")
				.model("X5")
				.type(Car.CarType.SUV)
				.pricePerDay(2500)
				.createCar();
		assertTrue(myDao.create(car1));

	}

    @Test
    public void updateCar() {
        Car car1 = new CarBuilder()
                .make("Mercedess")
                .model("ML 350")
                .type(Car.CarType.SUV)
                .pricePerDay(2000)
                .createCar();
        assertTrue(myDao.create(car1));

        Car car2 = new CarBuilder()
                .id(car1.getId())
                .make("Mazda")
                .model("RX7")
                .type(Car.CarType.Sedan)
                .pricePerDay(3000)
                .createCar();
        assertTrue(myDao.update(car2));
        assertEquals("Mazda", myDao.read(car1.getId()).getMake());
        assertEquals("RX7", myDao.read(car1.getId()).getModel());
        assertEquals(3000, myDao.read(car1.getId()).getPricePerDay());
        assertEquals(Car.CarType.Sedan, myDao.read(car1.getId()).getType());
    }

    @Test
    public void removeUser() {
        Car car = new CarBuilder()
                .make("Nisan")
                .model("Skyline")
                .type(Car.CarType.Sedan)
                .pricePerDay(5000)
                .createCar();
        assertTrue(myDao.create(car));
        assertTrue(myDao.delete(car.getId()));
        assertNull(myDao.read(car.getId()));
    }
}