package util;

import com.epam.rd.november.dao.impl.SimpleDaoFactory;
import com.epam.rd.november.models.*;
import com.epam.rd.november.models.builders.CarBuilder;
import com.epam.rd.november.models.builders.OrderBuilder;
import com.epam.rd.november.models.builders.UserBuilder;
import com.epam.rd.november.util.HashPassword;

import java.text.ParseException;
import java.text.SimpleDateFormat;


public class TestUtil {

	public static Delivery createDelivery() {
		Delivery delivery = new Delivery();
		delivery.setCity("Dnipro");
		delivery.setAddress("Koroleva 34");
		SimpleDaoFactory.getDaoFactory().getDeliveryDao().create(delivery);
		return delivery;
	}

	public static Crash createCrash() {
		Crash crash = new Crash();
		crash.setDamageSum(2000);
		crash.setCrashDescription("Car needs new spark plugs");
		crash.setIsPaid(false);
		SimpleDaoFactory.getDaoFactory().getCrashDao().create(crash);
		return crash;
	}

	public static Order createOrder(String user, String userLogin) {
        Order order = new OrderBuilder()
                .user(createUserForIT(user, userLogin))
                .car(createCar())
                .address(createDelivery())
                .crash(createCrash())
                .description("desk")
                .firstDay(createDate(1, 05, 2018))
                .lastDay(createDate(7, 05, 2018))
                .withDriver(true)
                .bill(2323)
                .status(Order.Status.CREATED)
                .createOrder();
        SimpleDaoFactory.getDaoFactory().getOrderDao().create(order);
        return order;
	}

	public static Car createCar() {
		Car car = new CarBuilder()
				.make("BmW")
				.model("Order")
				.type(Car.CarType.Sedan)
				.pricePerDay(500)
				.createCar();
		SimpleDaoFactory.getDaoFactory().getCarDao().create(car);
		return car;
	}


	public static User createUserForIT(String email, String login) {
		User user = new UserBuilder()
				.firstName("User")
				.lastName("Test")
				.login(login)
				.email(email)
				.password(HashPassword.hashPassword("Test1"))
				.createUser();
		SimpleDaoFactory.getDaoFactory().getUserDao().create(user);
		return user;
	}

	public static java.util.Date createDate(int day, int month, int year) {
		String date = year + "/" + month + "/" + day;
		java.util.Date newDate = null;

		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
			newDate = formatter.parse(date);
		} catch (ParseException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
		return newDate;
	}
}
